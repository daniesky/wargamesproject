package unitTests;

import edu.ntnu.idatt2001.daniesky.wargamesproject.unit.*;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class UnitFactoryTest {


    @Test
    public void createUnitTest(){
        Unit testUnit = UnitFactory.createUnit("INFANTRY UNIT","Bob",100);
        Unit testUnit2 = UnitFactory.createUnit("RANGED UNIT","Bob",80);
        Unit testUnit3= UnitFactory.createUnit("CAVALRY UNIT","Bob",110);
        Unit testUnit4 = UnitFactory.createUnit("COMMANDER UNIT","Bob",120);

        // Tests that the factory creates Infantry units properly.
        Assertions.assertInstanceOf(InfantryUnit.class,testUnit);
        Assertions.assertEquals("Bob",testUnit.getName());
        Assertions.assertEquals(100,testUnit.getHealth());

        // Tests that the factory creates Ranged units properly.
        Assertions.assertInstanceOf(RangedUnit.class,testUnit2);
        Assertions.assertEquals("Bob",testUnit2.getName());
        Assertions.assertEquals(80,testUnit2.getHealth());

        // Tests that the factory creates Cavalry units properly.
        Assertions.assertInstanceOf(CavalryUnit.class,testUnit3);
        Assertions.assertEquals("Bob",testUnit3.getName());
        Assertions.assertEquals(110,testUnit3.getHealth());

        // Tests that the factory creates Commander units properly.
        Assertions.assertInstanceOf(CommanderUnit.class,testUnit4);
        Assertions.assertEquals("Bob",testUnit4.getName());
        Assertions.assertEquals(120,testUnit4.getHealth());
    }

}
