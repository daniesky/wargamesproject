package unitTests;

import edu.ntnu.idatt2001.daniesky.wargamesproject.battle.Terrain;
import edu.ntnu.idatt2001.daniesky.wargamesproject.unit.InfantryUnit;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

/**
 * Test class for the Infantry unit. Here i only test the attack methods, as i see no reason to test
 * getters, setters and toStrings.
 */
public class InfantryUnitTest {

    @Test
    public void attackTest(){
        InfantryUnit testUnit = new InfantryUnit("Thomas",100);
        InfantryUnit testUnit2 = new InfantryUnit("Roger",100);
        testUnit.attack(testUnit2,Terrain.FOREST);
        assertEquals(93,testUnit2.getHealth());
    }

    @Test
    public void controlBonuses(){

        InfantryUnit testunit = new InfantryUnit("Bob",100);

        // Control bonuses for forest terrain.
        Assertions.assertEquals(4,testunit.getAttackBonus(Terrain.FOREST));
        Assertions.assertEquals(2,testunit.getResistBonus(Terrain.FOREST));

        // Control bonuses for hill terrain.
        Assertions.assertEquals(2,testunit.getAttackBonus(Terrain.HILL));
        Assertions.assertEquals(1,testunit.getResistBonus(Terrain.HILL));

        // Control bonuses for plain terrain.
        Assertions.assertEquals(2,testunit.getAttackBonus(Terrain.PLAINS));
        Assertions.assertEquals(1,testunit.getResistBonus(Terrain.PLAINS));
    }


    @ParameterizedTest
    @CsvSource({" '', 1, 1, 1",
            "health, -1, 1,1",
            "attack, 1, -1, 1",
            "armor, 1, 1, -1"})
    //Tests that all parameters in constructor throws exception for illegal arguments.
    public void constructor_throws_Exceptions(String name, int health, int attack, int armor){
        Assertions.assertThrows(IllegalArgumentException.class, () -> {
            var infantry = new InfantryUnit(name, health, attack, armor);
        });
    }


}