package unitTests;

import edu.ntnu.idatt2001.daniesky.wargamesproject.battle.Terrain;
import edu.ntnu.idatt2001.daniesky.wargamesproject.unit.CommanderUnit;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

import static org.junit.jupiter.api.Assertions.assertEquals;

/**
 * Test class for the Commander Unit. Test here is not really critical as the methods do not
 * defer from the Cavalry unit.
 */
public class CommanderUnitTest {
    @Test
    public void attackTest(){
        CommanderUnit testUnit = new CommanderUnit("Lebron",250);
        CommanderUnit testUnit2 = new CommanderUnit("Steph",250);
        testUnit.attack(testUnit2, Terrain.HILL);
        assertEquals(235,testUnit2.getHealth());
        testUnit.attack(testUnit2,Terrain.HILL);
        assertEquals(224,testUnit2.getHealth());

    }
    @ParameterizedTest
    @CsvSource({" '', 1, 1, 1",
            "health, -1, 1,1",
            "attack, 1, -1, 1",
            "armor, 1, 1, -1"})
    //Tests that all parameters in constructor throws exception for illegal arguments.
    public void constructor_throws_Exceptions(String name, int health, int attack, int armor){
        Assertions.assertThrows(IllegalArgumentException.class, () -> {
            var commander = new CommanderUnit(name, health, attack, armor);
        });
    }
}
