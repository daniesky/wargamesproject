package unitTests;

import edu.ntnu.idatt2001.daniesky.wargamesproject.battle.Terrain;
import edu.ntnu.idatt2001.daniesky.wargamesproject.unit.CavalryUnit;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

import static org.junit.jupiter.api.Assertions.assertEquals;

/**
 * Test class for the Cavalry unit. This also just tests the attack method as it is the only one that needs testing,
 * in my opinion.
 */
public class CavalryUnitTest {
    /**
     * The attack test here controls that the attack works as it should, and the attack bonus is working as it should
     * The first unit attacks twice and it checks if the health of the victim is calculated correctly.
     */
    @Test
    public void attackTest(){
        CavalryUnit unitTest = new CavalryUnit("Thomas",200);
        CavalryUnit unitTest2 = new CavalryUnit("Roger", 200);
        unitTest.attack(unitTest2, Terrain.HILL);
        // Health = 200 - (20 + (4+2)) + (12+1) = 187
        assertEquals(187, unitTest2.getHealth()); // First attack
        unitTest.attack(unitTest2,Terrain.HILL);
        // Health = 187 - (20+2) + (12+1) = 178
        assertEquals(178,unitTest2.getHealth()); // Second attack
    }

    @Test
    public void controlBonuses(){
        CavalryUnit unit = new CavalryUnit("Bob",100);
        System.out.println(unit.getUnitName());

        // Controlling bonuses in plain terrain.
        Assertions.assertEquals(8,unit.getAttackBonus(Terrain.PLAINS));
        Assertions.assertEquals(1,unit.getResistBonus(Terrain.PLAINS));

        // Controlling bonuses in forest terrain.
        Assertions.assertEquals(6,unit.getAttackBonus(Terrain.FOREST));
        Assertions.assertEquals(0,unit.getResistBonus(Terrain.FOREST));

        // Controlling bonuses in hill terrain.
        Assertions.assertEquals(6,unit.getAttackBonus(Terrain.HILL));
        Assertions.assertEquals(1,unit.getResistBonus(Terrain.HILL));
    }
    @ParameterizedTest
    @CsvSource({" '', 1, 1, 1",
            "health, -1, 1,1",
            "attack, 1, -1, 1",
            "armor, 1, 1, -1"})
    //Tests that all parameters in constructor throws exception for illegal arguments.
    public void constructor_throws_Exceptions(String name, int health, int attack, int armor){
        Assertions.assertThrows(IllegalArgumentException.class, () -> {
            var cavalryUnit = new CavalryUnit(name, health, attack, armor);
        });
    }
}
