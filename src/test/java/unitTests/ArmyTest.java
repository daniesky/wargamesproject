package unitTests;

import edu.ntnu.idatt2001.daniesky.wargamesproject.unit.*;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.List;

public class ArmyTest {
    @Test
    public void addAndRemoveTest(){
        Army army = new Army("Test Army");
        Unit testUnit = new InfantryUnit("Footman",100);
        army.add(testUnit);
        Assertions.assertTrue(army.getUnits().contains(testUnit));
        army.remove(testUnit);
        Assertions.assertFalse(army.getUnits().contains(testUnit));
    }

    @Test
    public void testGetSpecificUnit(){
        Army army = new Army("Test Army");
        Unit infantryUnit = new InfantryUnit("Footman",100);
        Unit infantryUnitTwo = new InfantryUnit("Footman",100);
        Unit rangedUnit = new RangedUnit("Archer",80);
        Unit rangedUnitTwo = new RangedUnit("Archer",80);
        Unit cavalryUnit = new CavalryUnit("Horseman",100);
        Unit cavalryUnitTwo = new CavalryUnit("Horseman",100);
        Unit commanderUnit = new CommanderUnit("Horse General",100);
        Unit commanderUnitTwo = new CommanderUnit("Horse General",100);
        army.add(infantryUnit);
        army.add(infantryUnitTwo);
        army.add(rangedUnit);
        army.add(rangedUnitTwo);
        army.add(cavalryUnit);
        army.add(cavalryUnitTwo);
        army.add(commanderUnit);
        army.add(commanderUnitTwo);
        List<Unit> testUnits = army.getInfantryUnits();
        Assertions.assertTrue((testUnits.contains(infantryUnit) && testUnits.contains(infantryUnitTwo)));
        testUnits = army.getRangedUnits();
        Assertions.assertTrue(testUnits.contains(rangedUnit) && testUnits.contains(rangedUnitTwo));
        testUnits = army.getCavalryUnits();
        Assertions.assertTrue(testUnits.contains(cavalryUnit) && testUnits.contains(cavalryUnitTwo));
        testUnits = army.getCommanderUnits();
        Assertions.assertTrue(testUnits.contains(commanderUnit) && testUnits.contains(commanderUnitTwo));
    }





}
