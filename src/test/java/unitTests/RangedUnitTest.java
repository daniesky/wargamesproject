package unitTests;

import edu.ntnu.idatt2001.daniesky.wargamesproject.battle.Terrain;
import edu.ntnu.idatt2001.daniesky.wargamesproject.unit.RangedUnit;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

import static org.junit.jupiter.api.Assertions.assertEquals;

/**
 * Test class for the Ranged unit. Only tests the attack method.
 */
public class RangedUnitTest {
    @Test
    /**
     * This testmethod tests the attack function, and the ranged units declining defense.
     * Therefore the unit is attacked four times.
     */
    public void attackTest(){
        RangedUnit unitTest = new RangedUnit("Derek",75);
        RangedUnit unitTest2 = new RangedUnit("OBJ",75);
        // 6 bonus defense.
        unitTest.attack(unitTest2,Terrain.PLAINS);
        assertEquals(71, unitTest2.getHealth());
        // 4 bonus defense.
        unitTest.attack(unitTest2,Terrain.PLAINS);
        assertEquals(65,unitTest2.getHealth());
        // 2 bonus defense for the remaining attacks.
        unitTest.attack(unitTest2,Terrain.PLAINS);
        assertEquals(57, unitTest2.getHealth());
        unitTest.attack(unitTest2,Terrain.PLAINS);
        assertEquals(49,unitTest2.getHealth());
    }

    @Test
    public void controlBonuses(){
        RangedUnit unit = new RangedUnit("Bob",100);

        Assertions.assertEquals(1, unit.getAttackBonus(Terrain.FOREST));

        Assertions.assertEquals(3, unit.getAttackBonus(Terrain.PLAINS));

        Assertions.assertEquals(5,unit.getAttackBonus(Terrain.HILL));

    }
    @ParameterizedTest
    @CsvSource({" '', 1, 1, 1",
            "health, -1, 1,1",
            "attack, 1, -1, 1",
            "armor, 1, 1, -1"})
    //Tests that all parameters in constructor throws exception for illegal arguments.
    public void constructor_throws_Exceptions(String name, int health, int attack, int armor){
        Assertions.assertThrows(IllegalArgumentException.class, () -> {
            var ranged = new RangedUnit(name, health, attack, armor);
        });
    }
}
