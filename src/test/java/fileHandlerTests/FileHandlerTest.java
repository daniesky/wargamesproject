package fileHandlerTests;

import edu.ntnu.idatt2001.daniesky.wargamesproject.fileHandling.FileHandler;
import edu.ntnu.idatt2001.daniesky.wargamesproject.unit.Army;
import edu.ntnu.idatt2001.daniesky.wargamesproject.unit.CommanderUnit;
import edu.ntnu.idatt2001.daniesky.wargamesproject.unit.InfantryUnit;
import edu.ntnu.idatt2001.daniesky.wargamesproject.unit.Unit;
import javafx.fxml.FXML;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.io.File;

public class FileHandlerTest {
    @Test
    public void readFileTest(){
        //Creates filehandler object andd necessary army and units.
        FileHandler f = new FileHandler("Armies/");
        Unit unit1 = new InfantryUnit("Footman",100);
        Unit unit2 = new InfantryUnit("Footman",100);
        Unit unit3 = new CommanderUnit("Mountain King",180);
        Army army = new Army("Test");
        army.add(unit1);
        army.add(unit2);
        army.add(unit3);
        f.createArmyFile(army);

        File file = new File("Armies/Test.csv");
        Army testArmy = f.readArmyFile(file);
        Assertions.assertEquals(3,testArmy.getUnits().size());
        Assertions.assertEquals("Test",testArmy.getName());
        /*
        The reason for using toString for comparing is the units created
        are not the same objects, therefore the equals method will fail. The test
        checks that the units have the same object variables, and therefore has
        been read properly.
         */
        Assertions.assertEquals(unit1.toString(),testArmy.getUnits().get(0).toString());
        Assertions.assertEquals(unit2.toString(),testArmy.getUnits().get(1).toString());
        Assertions.assertEquals(unit3.toString(),testArmy.getUnits().get(2).toString());
        //Testing that the units are not all equal.
        Assertions.assertNotEquals(unit1.toString(),testArmy.getUnits().get(2).toString());
        Assertions.assertNotEquals(unit2.toString(),testArmy.getUnits().get(2).toString());

        //Deletes file when testing is done.
        file.delete();
    }

    @Test
    public void writeFileTest(){
        //Creates filehandler object andd necessary army and units.
        FileHandler f = new FileHandler("Armies/");
        Army testArmy = new Army("Test");
        testArmy.add(new InfantryUnit("Footman",100));
        testArmy.add(new InfantryUnit("Footman",100));
        testArmy.add(new CommanderUnit("Mountain King",180));
        //Creates the file and writes army to it.
        boolean write = f.createArmyFile(testArmy);

        //Reads the same army back and compares the two armies.
        Army controlArmy = f.readArmyFile(new File("Armies/Test.csv"));

        // We control that the armies are equal by comparing their toStrings, as they have equal units but not the same units.
        Assertions.assertEquals(testArmy.toString(),controlArmy.toString());

        //Controls if the file was created.
        Assertions.assertTrue(write);

        //Deletes file when testing is done.
        File file = new File("Armies/Test.csv");
        file.delete();
    }

    @Test
    public void wrongFileTypeTest(){
        FileHandler f = new FileHandler("Armies/");
        File file = new File("readTest.txt");
        Assertions.assertThrows(IllegalArgumentException.class, () -> f.readArmyFile(file));
    }

    @Test
    public void updateArmyFileTest(){
        //Creates filehandler object andd necessary army and units.
        FileHandler f = new FileHandler("Armies/");
        Army testArmy = new Army("Test");
        testArmy.add(new InfantryUnit("Footman",100));
        testArmy.add(new InfantryUnit("Footman",100));
        testArmy.add(new CommanderUnit("Mountain King",180));
        testArmy.add(new InfantryUnit("Footman",100));
        testArmy.add(new InfantryUnit("Footman",100));
        testArmy.add(new CommanderUnit("Mountain King",180));

        Army controlArmy = new Army("Test2");
        controlArmy.add(new InfantryUnit("Footman",100));
        controlArmy.add(new InfantryUnit("Footman",100));
        controlArmy.add(new CommanderUnit("Mountain King",180));
        controlArmy.add(new InfantryUnit("Footman",100));
        controlArmy.add(new InfantryUnit("Footman",100));
        controlArmy.add(new CommanderUnit("Mountain King",180));
        controlArmy.add(new CommanderUnit("Lord Sky",1000));

        //Creates the file for the first army.
        f.createArmyFile(testArmy);

        //Changes name and adds a unit to testArmy.
        testArmy.setName("Test2");
        testArmy.add(new CommanderUnit("Lord Sky",1000));
        //Updates the army.
        f.updateFile(testArmy);

        //Then army is read again, and if the two armies are equal, then the update has been successful.
        Army read = f.readArmyFile(new File(testArmy.getPath()));
        Assertions.assertEquals(testArmy.toString(),read.toString());
        File file = new File(testArmy.getPath());
        //Deletes file when testing is done.
        file.delete();
    }
}
