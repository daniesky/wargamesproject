package battleTests;

import edu.ntnu.idatt2001.daniesky.wargamesproject.battle.Terrain;
import edu.ntnu.idatt2001.daniesky.wargamesproject.unit.Army;
import edu.ntnu.idatt2001.daniesky.wargamesproject.battle.Battle;
import edu.ntnu.idatt2001.daniesky.wargamesproject.unit.CommanderUnit;
import edu.ntnu.idatt2001.daniesky.wargamesproject.unit.InfantryUnit;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.Assertions;

/**
 * Test class for the Battle class. Here i feel it is necessary to test the simulation of the battle.
 */
public class BattleTest {
    /**
     * If we simulate a full scale battle then we cannot predict the outcome, making it difficult to test the method.
     * To test if the method returns the victorious army correctly we simulate a battle with one of the armies empty.
     * In theory the army with the soldier will return victorious. We run it with the first and second army to ensure
     * that it works with both as victorious.
     */
    @Test
    public void testWinSimulate(){
        Army army1 = new Army("Orcs");
        Army army2 = new Army("Humans");
        //With these health numbers, army 1 is basically guaranteed to win.
        army1.add(new InfantryUnit("Footman",100000));
        army2.add(new InfantryUnit("Footman",1));
        Battle testBattle = new Battle(army1,army2, Terrain.PLAINS);
        testBattle.simulate(false,false);
        Army testWinner = testBattle.getWinner();
        Assertions.assertTrue(testWinner.equals(army1));
        Army army3 = new Army("Orcs");
        Army army4 = new Army("Humans");
        army4.add(new InfantryUnit("Footman",100000));
        army3.add(new InfantryUnit("Footman",1));
        Battle testBattleTwo = new Battle(army4,army3,Terrain.PLAINS);
        testBattleTwo.simulate(false,false);
        Army testWinnerTwo = testBattleTwo.getWinner();
        Assertions.assertTrue(testWinnerTwo.equals(army4));

    }

    /**
     * Another test for the simulation will be a full scale battle, and the tests verify that one of the armies has no
     * troops when the battle is over, and that the other army is then victorious.
     */
    @Test
    public void fullSimulationTest(){
        Army army1 = new Army("Orcs");
        Army army2 = new Army("Humans");
        for(int i = 0; i<5;i++){
            army1.add(new InfantryUnit("Grunt",100));
            army2.add(new InfantryUnit("Footman",100));
        }
        army1.add(new CommanderUnit("Gul'dan",180));
        army2.add(new CommanderUnit("Mountain King",180));
        Battle testBattle = new Battle(army1,army2,Terrain.PLAINS);
        testBattle.simulate(false,false);
        Army winner = testBattle.getWinner();
        Assertions.assertTrue(!army1.hasLivingUnits() || !army2.hasLivingUnits());
        if(army1.hasLivingUnits()){
            Assertions.assertTrue(winner.equals(army1));
        }
        if(army2.hasLivingUnits()){
            Assertions.assertTrue(winner.equals(army2));
        }
    }
}
