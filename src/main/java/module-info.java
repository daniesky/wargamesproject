module WargamesProject {
    requires javafx.controls;
    requires javafx.fxml;
    requires org.controlsfx.controls;


    exports edu.ntnu.idatt2001.daniesky.wargamesproject.unit;
    opens edu.ntnu.idatt2001.daniesky.wargamesproject.unit to javafx.base;

    exports edu.ntnu.idatt2001.daniesky.wargamesproject.gui.controllers;
    opens edu.ntnu.idatt2001.daniesky.wargamesproject.gui.controllers to javafx.fxml;

    exports edu.ntnu.idatt2001.daniesky.wargamesproject.gui.models;
    opens  edu.ntnu.idatt2001.daniesky.wargamesproject.gui.models to javafx.base,javafx.fxml;

    exports edu.ntnu.idatt2001.daniesky.wargamesproject.gui.main;
    opens edu.ntnu.idatt2001.daniesky.wargamesproject.gui.main to javafx.fxml;

}