package edu.ntnu.idatt2001.daniesky.wargamesproject.unit;

import edu.ntnu.idatt2001.daniesky.wargamesproject.battle.Terrain;

/**
 * Infantry unit subclass.
 */
public class InfantryUnit extends Unit {
    /**
     * Constructor with all four parameters.
     * @param name
     * @param health
     * @param attack
     * @param armor
     */
    public InfantryUnit(String name, int health, int attack, int armor) {
        super(name, health, attack, armor);
    }

    /**
     * Constructor with attack and armor stat already defined.
     * @param name
     * @param health
     */
    public InfantryUnit(String name, int health) {
        super(name, health, 15, 10);
    }

    @Override
    /**
     * Returns attack bonus. Unit has advantage in forest terrain.
     */
    public int getAttackBonus(Terrain terrain) {
        if(terrain == Terrain.FOREST){
            return 4;
        }
        else {
            return 2;
        }
    }

    @Override
    /**
     * Returns armor bonus.
     */
    public int getResistBonus(Terrain terrain) {
        if(terrain == Terrain.FOREST){
            return 2;
        }
        else {
            return 1;
        }
    }
}
