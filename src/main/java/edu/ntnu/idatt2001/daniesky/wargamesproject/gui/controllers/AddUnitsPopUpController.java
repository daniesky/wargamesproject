package edu.ntnu.idatt2001.daniesky.wargamesproject.gui.controllers;

import edu.ntnu.idatt2001.daniesky.wargamesproject.unit.Unit;
import edu.ntnu.idatt2001.daniesky.wargamesproject.unit.UnitFactory;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.TextField;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;

/**
 * Controller that handles the add units pop up.
 */
public class AddUnitsPopUpController implements Initializable{

    @FXML private ChoiceBox unitType;
    @FXML private TextField health;
    @FXML private TextField name;
    @FXML private TextField units;

    //Standard value for army to add to. This is set when the popup is called in different places.
    public static int army = 0;


    public ArrayList<Unit> unitsCreated;

    /**
     * Initializes the unitType dropdown menu.
     * @param url
     * @param resourceBundle
     */
    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        unitType.getItems().add("Infantry Unit");
        unitType.getItems().add("Ranged Unit");
        unitType.getItems().add("Cavalry Unit");
        unitType.getItems().add("Commander Unit");

    }

    /**
     * When all inputs are given and done button is pressed, units are created and returned to the army that is selected.
     * @param actionEvent
     * @throws IOException  if FXML loading error occurs.
     */
    public void doneButton(ActionEvent actionEvent) throws IOException {
        String type = unitType.getSelectionModel().getSelectedItem().toString();
        int health = Integer.parseInt(this.health.getText());
        String name = this.name.getText();
        int units = Integer.parseInt(this.units.getText());
        unitsCreated = UnitFactory.createUnits(type, name, health, units);
        switch(army){
            case 1:
                CreateArmiesController.unitDataOne.addAll(unitsCreated);
                break;
            case 2:
                CreateArmiesController.unitDataTwo.addAll(unitsCreated);
                break;
            case 3:
                EditArmyController.unitData.addAll(unitsCreated);
                EditArmyController.army.addAll(unitsCreated);
                break;
            case 4:
                EditMyArmyController.unitData.addAll(unitsCreated);
                break;
            case 5:
                NewArmyController.unitData.addAll(unitsCreated);
                break;
        }



    }
}
