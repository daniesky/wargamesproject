package edu.ntnu.idatt2001.daniesky.wargamesproject.unit;

/**
 * Subclass that describes the Commander unit.
 */
public class CommanderUnit extends CavalryUnit{
    /**
     * Constructor with 4 parameters.
     * @param name
     * @param health
     * @param attack
     * @param armor
     */
    public CommanderUnit(String name, int health, int attack, int armor) {
        super(name, health, attack, armor);
    }

    /**
     * Constructor with attack and armor predefined.
     * @param name
     * @param health
     */
    public CommanderUnit(String name, int health) {
        super(name, health,25,15);

    }
}
