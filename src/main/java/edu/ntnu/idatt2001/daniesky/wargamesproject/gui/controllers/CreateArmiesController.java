package edu.ntnu.idatt2001.daniesky.wargamesproject.gui.controllers;

import edu.ntnu.idatt2001.daniesky.wargamesproject.battle.Battle;
import edu.ntnu.idatt2001.daniesky.wargamesproject.battle.Terrain;
import edu.ntnu.idatt2001.daniesky.wargamesproject.fileHandling.FileHandler;
import edu.ntnu.idatt2001.daniesky.wargamesproject.gui.FXMLLoaderClass;
import edu.ntnu.idatt2001.daniesky.wargamesproject.unit.Army;
import edu.ntnu.idatt2001.daniesky.wargamesproject.unit.Unit;
import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.AnchorPane;
import javafx.stage.*;
import javafx.util.Duration;
import org.controlsfx.control.PopOver;
import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Optional;
import java.util.ResourceBundle;

/**
 * Controller for the "create armies" FXML file.
 */
public class CreateArmiesController implements Initializable{

    //TextFields containing the names of armies.
    @FXML private TextField firstArmyLabel;
    @FXML private TextField secondArmyLabel;

    //Tableview for army one, with columns.
    @FXML private TableView<Unit> firstArmyTable;
    @FXML private TableColumn <Unit, String> unitColumnOne;
    @FXML private TableColumn <Unit, String> unitNameOne;
    @FXML private TableColumn <Unit, Integer> unitHPOne;
    @FXML private TableColumn <Unit, Integer> unitATOne;
    @FXML private TableColumn <Unit, Integer> unitAROne;

    //Static lists that are used in add units pop up.
    public static ObservableList<Unit> unitDataOne = FXCollections.observableArrayList();
    public static ObservableList<Unit> unitDataTwo = FXCollections.observableArrayList();

    //Keeps track of armies that are imported.
    private String importPathOne;
    private String importPathTwo;

    //Tableview for army two, with columns.
    @FXML private TableView<Unit> secondArmyTable;
    @FXML private TableColumn <Unit, String> unitColumnTwo;
    @FXML private TableColumn <Unit, String> unitNameTwo;
    @FXML private TableColumn <Unit, Integer> unitHPTwo;
    @FXML private TableColumn <Unit, Integer> unitATTwo;
    @FXML private TableColumn <Unit, Integer> unitARTwo;

    @FXML private Button addUnitsOne;
    @FXML private Button addUnitsTwo;
    @FXML private CheckBox forestBox;
    @FXML private CheckBox hillBox;
    @FXML private CheckBox plainBox;
    @FXML private Button myArmiesOne;
    @FXML private Label saveFeedBack;
    @FXML private Label saveFeedBack2;

    //My armies popover, is static so that it can be closed from SelectMyArmyController.
    public static PopOver importPopOver = new PopOver();

    /**
     * Initializes the tables.
     * @param url
     * @param resourceBundle
     */
    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        //Initalize tables, so that they can retrieve correct information for Unit class.
        unitNameOne.setCellValueFactory(new PropertyValueFactory<>("name"));
        unitHPOne.setCellValueFactory(new PropertyValueFactory<>("health"));
        unitATOne.setCellValueFactory(new PropertyValueFactory<>("attack"));
        unitAROne.setCellValueFactory(new PropertyValueFactory<>("armor"));
        unitColumnOne.setCellValueFactory(new PropertyValueFactory<>("unitName"));
        refreshTableOne();
        //Initalize tables, so that they can retrieve correct information for Unit class.
        unitNameTwo.setCellValueFactory(new PropertyValueFactory<>("name"));
        unitHPTwo.setCellValueFactory(new PropertyValueFactory<>("health"));
        unitATTwo.setCellValueFactory(new PropertyValueFactory<>("attack"));
        unitARTwo.setCellValueFactory(new PropertyValueFactory<>("armor"));
        unitColumnTwo.setCellValueFactory(new PropertyValueFactory<>("unitName"));
        refreshTableTwo();
    }

    /**
     * Private method that displays the add units popup for the different armies.
     * @param armyClicked parameter passed, representing if the button is pressed for first or second army.
     * @throws IOException if FXML loading error occurs.
     */
    @FXML
    private void showAddUnitPopUp(int armyClicked)throws IOException{
        AnchorPane content = new AnchorPane();
        PopOver popOver = new PopOver();
        URL url = getClass().getClassLoader().getResource("addUnitsPopUp.fxml");
        content.getChildren().setAll((Node) FXMLLoader.load(url));
        popOver.setContentNode(content);
        switch(armyClicked){
            case 1:
                popOver.show(addUnitsOne);
                AddUnitsPopUpController.army = 1;
                break;
            case 2:
                popOver.show(addUnitsTwo);
                AddUnitsPopUpController.army = 2;
                break;
        }
    }

    /**
     * Shows the add units pop up for army  one.
     * @param actionEvent
     * @throws IOException if FXML loading error occurs.
     */
    @FXML
    public void showAddUnitPopUpOne(ActionEvent actionEvent) throws IOException {
        this.showAddUnitPopUp(1);
    }
    /**
     * Shows the add units pop up for army two.
     * @param actionEvent
     * @throws IOException if FXML loading error occurs.
     */
    @FXML
    public void showAddUnitPopUpTwo(ActionEvent actionEvent) throws IOException {
        this.showAddUnitPopUp(2);
    }
    /**
     * Resets items in table one.
     */
    @FXML
    public void refreshTableOne(){
        firstArmyTable.setItems(unitDataOne);
    }

    /**
     * Resets items in table two.
     */
    @FXML
    public void refreshTableTwo(){
        secondArmyTable.setItems(unitDataTwo);
    }

    /**
     * Removes units in table one.
     * @param actionEvent
     */
    @FXML
    public void removeOne(ActionEvent actionEvent){
        Unit selectedUnit = firstArmyTable.getSelectionModel().getSelectedItem();
        unitDataOne.remove(selectedUnit);
        this.refreshTableOne();
    }
    /**
     * Removes units in table two.
     * @param actionEvent
     */
    @FXML
    public void removeTwo(ActionEvent actionEvent){
        Unit selectedUnit = secondArmyTable.getSelectionModel().getSelectedItem();
        unitDataTwo.remove(selectedUnit);
        this.refreshTableTwo();
    }

    /**
     * Method that displays a pop-up window that allows u to select file. Returns the army read from the selected file.
     * @return returns the army uploaded.
     */
    private Army uploadArmy(){
        Stage stage = new Stage();
        FileChooser fileChooser = new FileChooser();
        fileChooser.setTitle("Open Army File");
        File f = fileChooser.showOpenDialog(stage);
        FileHandler fileHandler = new FileHandler("Armies/");
        try {
            Army army = fileHandler.readArmyFile(f);
            army.setImportPath(f.getAbsolutePath());
            return army;
        }catch(IllegalArgumentException e){
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Error occured");
            alert.setHeaderText("An error occured while reading the file");
            alert.setContentText("Something is wrong with uploaded file. It might be corrupt or in the wrong format. ");
            alert.showAndWait();
        }
        //Simply returns zero because upload was cancelled. No error needed.
        catch(NullPointerException e){
            return null;
        }
        return null;
    }

    /**
     * Method that saves the current army to the file that said army was imported from. Does not work if army is not
     * imported.
     * @param army army being saved.
     */
    private void saveToCurrentFile(Army army){
        if(army.getPath() == null){
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("File not found");
            alert.setHeaderText("This army is not imported.");
            alert.setContentText("Army is not imported, try saving to new file or my armies.");
            alert.showAndWait();
        }
        else{
            try {
                FileHandler fileHandler = new FileHandler("Armies/");
                fileHandler.updateFile(army);
            }
            catch(IllegalArgumentException e){
                Alert alert = new Alert(Alert.AlertType.ERROR);
                alert.setTitle("Fail save");
                alert.setHeaderText("Could not save army to current file.");
                alert.setContentText("Could not write to the specified file, file might be wrong format or corrupt.");
                alert.showAndWait();
            }
            catch(NullPointerException e){
                Alert alert = new Alert(Alert.AlertType.ERROR);
                alert.setTitle("Fail save");
                alert.setHeaderText("Could not save army to current file.");
                alert.setContentText("Could not find file. File might have been " +
                        "deleted or moved from the directory it was imported from. ");
                alert.showAndWait();
            }
        }
    }

    /**
     * Saves army to a new file that user can choose where to place.
     * @param army army that is being saved to new  file.
     */
    private void saveToNewFile(Army army){
        Stage stage = new Stage();
        FileChooser fileChooser = new FileChooser();
        FileChooser.ExtensionFilter extFilter = new FileChooser.ExtensionFilter("CSV files (*.csv)", "*.csv");
        fileChooser.getExtensionFilters().add(extFilter);
        fileChooser.setTitle("Save army file");
        fileChooser.setInitialFileName(army.getName().replaceAll(" ",""));
        File file = fileChooser.showSaveDialog(stage);
        try {
            file.createNewFile();
            FileHandler fileHandler = new FileHandler("Armies/");
            army.setImportPath(file.getAbsolutePath());
            fileHandler.writeToArmyFile(file,army);
        }catch(IOException e){
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Fail save");
            alert.setHeaderText("Could not save army to new file.");
            alert.setContentText("Something went wrong when creating file.");
            alert.showAndWait();
        }

    }

    /**
     * Method that displays a text label that says successful save.
     * @param armySaved
     */
    @FXML
    private void successfulSave(int armySaved){
        switch (armySaved) {
            case 1:
                Timeline t = new Timeline(
                        new KeyFrame(Duration.ZERO, new EventHandler<ActionEvent>() {
                    @Override
                    public void handle(ActionEvent actionEvent) {
                        saveFeedBack.setText("Successful save!");
                    }
                }),
                        new KeyFrame(Duration.seconds(2), new EventHandler<ActionEvent>() {
                    @Override
                    public void handle(ActionEvent actionEvent) {
                        saveFeedBack.setText("");
                    }
                }));
                t.play();
                break;
            case 2:
                Timeline t2 = new Timeline(
                        new KeyFrame(Duration.ZERO, new EventHandler<ActionEvent>() {
                    @Override
                    public void handle(ActionEvent actionEvent) {
                        saveFeedBack2.setText("Successful save!");
                    }
                }),
                        new KeyFrame(Duration.seconds(2), new EventHandler<ActionEvent>() {
                    @Override
                    public void handle(ActionEvent actionEvent) {
                        saveFeedBack2.setText("");
                    }
                }));
                t2.play();
                break;
        }
    }

    /**
     * Saves the army to my armies.
     * @param army army that is being saved.
     */
    private void saveToMyArmies(Army army){
        FileHandler fileHandler = new FileHandler("Armies/");
        try {
            fileHandler.createArmyFile(army);
        }catch(IllegalArgumentException e){
            Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
            alert.setTitle("Fail save");
            alert.setHeaderText("Army file already exists!");
            alert.setContentText("An army with this name already exists in your armies!" +
                    "Would you like to overwrite this army with your current army?");
            Optional<ButtonType> option = alert.showAndWait();

            if(option.get() == ButtonType.OK){
                fileHandler.updateFile(army);
            }
        }

    }

    /**
     * Uses saveToCurrentFile when "save to current file" button one is clicked.
     * @param actionEvent
     */
    @FXML
    public void saveToCurrentOne(ActionEvent actionEvent){
        try {
            Army army = new Army(firstArmyLabel.getText(), new ArrayList<>(unitDataOne));
            army.setImportPath(importPathOne);
            this.saveToCurrentFile(army);
            successfulSave(1);
        }
        catch(IllegalArgumentException e){
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Fail save");
            alert.setHeaderText("Army does not have name/units");
            alert.setContentText("App can not save without army name or units.");
            alert.showAndWait();
        }

    }

    /**
     * Uses saveToNewFile method for the first army.
     * @param actionEvent
     */
    @FXML
    public void saveToNewFileOne(ActionEvent actionEvent){
        try {
            Army army = new Army(firstArmyLabel.getText(), new ArrayList<>(unitDataOne));
            this.saveToNewFile(army);
            this.successfulSave(1);
        }
        catch(IllegalArgumentException e){
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Fail save");
            alert.setHeaderText("Army does not have name/units");
            alert.setContentText("App can not save without army name or units.");
            alert.showAndWait();
        }
    }

    /**
     * Uses saveToMyArmies method for army one.
     * @param actionEvent
     */
    @FXML
    public void saveToMyArmiesOne(ActionEvent actionEvent){
        try {
            Army army = new Army(firstArmyLabel.getText(), new ArrayList<>(unitDataOne));
            this.saveToMyArmies(army);
            successfulSave(1);
        }
        catch(IllegalArgumentException e){
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Fail save");
            alert.setHeaderText("Army does not have name/units");
            alert.setContentText("App can not save without army name or units.");
            alert.showAndWait();
        }
    }
    /**
     * Uses saveToCurrentFile when "save to current file" button two is clicked.
     * @param actionEvent
     */
    @FXML
    public void saveToCurrentTwo(ActionEvent actionEvent){
        try {
            Army army = new Army(secondArmyLabel.getText(), new ArrayList<>(unitDataTwo));
            army.setImportPath(importPathTwo);
            this.saveToCurrentFile(army);
            successfulSave(2);

        }
        catch(IllegalArgumentException e){
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Fail save");
            alert.setHeaderText("Army does not have name/units");
            alert.setContentText("App can not save without army name or units.");
            alert.showAndWait();
        }

    }

    /**
     * Uses saveToNewFile method for the second army.
     * @param actionEvent
     */
    @FXML
    public void saveToNewFileTwo(ActionEvent actionEvent){
        try {
            Army army = new Army(secondArmyLabel.getText(), new ArrayList<>(unitDataTwo));
            this.saveToNewFile(army);
            successfulSave(2);
        }
        catch(IllegalArgumentException e){
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Fail save");
            alert.setHeaderText("Army does not have name/units");
            alert.setContentText("App can not save without army name or units.");
            alert.showAndWait();
        }

    }

    /**
     * Uses saveToMyArmies method for army two.
     * @param actionEvent
     */
    @FXML
    public void saveToMyArmiesTwo(ActionEvent actionEvent){
        try {
            Army army = new Army(secondArmyLabel.getText(), new ArrayList<>(unitDataTwo));
            this.saveToMyArmies(army);
            successfulSave(2);
        }
        catch(IllegalArgumentException e){
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Fail save");
            alert.setHeaderText("Army does not have name/units");
            alert.setContentText("App can not save without army name or units.");
            alert.showAndWait();
        }
    }



    /**
     * Upload army button one is clicked. Calls uploadArmy method and updates the table and name field.
     * @param actionEvent
     */
    @FXML
    public void uploadArmyOne(ActionEvent actionEvent){
        Army army = this.uploadArmy();
        if(army != null) {
            firstArmyLabel.setText(army.getName());
            unitDataOne.clear();
            unitDataOne.addAll(army.getUnits());
            importPathOne = army.getPath();
            this.refreshTableOne();
        }
    }

    /**
     * Upload army button two is clicked. Calls uploadArmy method and updates the table and name field.
     * @param actionEvent
     */
    @FXML
    public void uploadArmyTwo(ActionEvent actionEvent){
            Army army = this.uploadArmy();
        if(army != null) {
            secondArmyLabel.setText(army.getName());
            unitDataTwo.clear();
            unitDataTwo.addAll(army.getUnits());
            importPathTwo = army.getPath();
            this.refreshTableTwo();
        }
        
    }
    /**
     * Handles the forest terrain box. If forest is selected, all others are deselected.
     * @param actionEvent
     */
    @FXML
    public void handleForestBox(ActionEvent actionEvent){
        forestBox.setSelected(true);
        hillBox.setSelected(false);
        plainBox.setSelected(false);
    }
    /**
     * Handles the hill terrain box. If hill is selected, all others are deselected.
     * @param actionEvent
     */
    @FXML
    public void handleHillBox(ActionEvent actionEvent){
        forestBox.setSelected(false);
        hillBox.setSelected(true);
        plainBox.setSelected(false);
    }
    /**
     * Handles the plains terrain box. If plains is selected, all others are deselected.
     * @param actionEvent
     */
    @FXML
    public void handlePlainBox(ActionEvent actionEvent){
        forestBox.setSelected(false);
        hillBox.setSelected(false);
        plainBox.setSelected(true);
    }

    /**
     * Private method that displays popup with table containing all armies within "my armies". Popover retrieves
     * selected army and table from SelectMyArmyController and then imports army into this GUI controller.
     * @throws IOException if FXML loading error occurs.
     */
    private void importMyArmy() throws IOException {
        AnchorPane anchorPane = new AnchorPane();
        URL url = getClass().getClassLoader().getResource("selectMyArmy.fxml");
        anchorPane.getChildren().setAll((Node) FXMLLoader.load(url));
        importPopOver.setContentNode(anchorPane);
        importPopOver.detach();
        importPopOver.setAnchorLocation(PopupWindow.AnchorLocation.CONTENT_BOTTOM_LEFT);
        importPopOver.show(myArmiesOne);
        importPopOver.setOnHidden(new EventHandler<WindowEvent>() {
            @Override
            public void handle(WindowEvent windowEvent) {
                if(SelectMyArmyController.selectedArmy != null){
                    Army army = SelectMyArmyController.selectedArmy;
                    switch(SelectMyArmyController.selectedTable){
                        case 0:
                            firstArmyLabel.setText(army.getName());
                            unitDataOne.clear();
                            unitDataOne.addAll(army.getUnits());
                            importPathOne = army.getPath();
                            SelectMyArmyController.selectedArmy = null;
                            SelectMyArmyController.selectedTable = 2;
                            break;
                        case 1:
                            secondArmyLabel.setText(army.getName());
                            unitDataTwo.clear();
                            unitDataTwo.addAll(army.getUnits());
                            importPathTwo = army.getPath();
                            SelectMyArmyController.selectedArmy = null;
                            SelectMyArmyController.selectedTable = 2;
                            break;
                    }
                }
            }
        });

    }

    /**
     * Uses importMyArmy method for army one. Sets static variable selectedTable in SelectMyController to 0, so that
     * importMyArmy method knows where to import army.
     * @param actionEvent
     * @throws IOException if FXML loading error occurs.
     */
    @FXML
    public void myArmyOne(ActionEvent actionEvent) throws IOException {
        SelectMyArmyController.selectedTable = 0;
        this.importMyArmy();
    }
    /**
     * Uses importMyArmy method for army two. Sets static variable selectedTable in SelectMyController to 1, so that
     * importMyArmy method knows where to import army.
     * @param actionEvent
     * @throws IOException if FXML loading error occurs.
     */
    @FXML
    public void myArmyTwo(ActionEvent actionEvent) throws IOException{
        SelectMyArmyController.selectedTable = 1;
        this.importMyArmy();
    }


    /**
     * Done creating army method, creates both armies and sets the static battle in Battle Simulation controller,
     * then loads said controller.
     * @param actionEvent
     * @throws IOException if FXML loading error occurs.
     */
    @FXML
    public void doneCreating(ActionEvent actionEvent) throws IOException {
        try {
            String armyOneName = firstArmyLabel.getText();
            ArrayList<Unit> unitsOne = new ArrayList<>(firstArmyTable.getItems());
            Army armyOne = new Army(armyOneName, unitsOne);
            armyOne.setImportPath(importPathOne);

            String armyTwoName = secondArmyLabel.getText();
            ArrayList<Unit> unitsTwo = new ArrayList<>(secondArmyTable.getItems());
            Army armyTwo = new Army(armyTwoName, unitsTwo);
            armyTwo.setImportPath(importPathTwo);

            Terrain terrain = null;
            if (forestBox.isSelected()) {
                terrain = Terrain.FOREST;
            }
            if (hillBox.isSelected()) {
                terrain = Terrain.HILL;
            }
            if (plainBox.isSelected()) {
                terrain = Terrain.PLAINS;
            }
            if (terrain == null) {
                Alert alert = new Alert(Alert.AlertType.ERROR);
                alert.setTitle("Error occured!");
                alert.setHeaderText("Terrain is not selected. ");
                alert.setContentText("Select terrain in order to proceed!");
                alert.showAndWait();
                throw new IllegalArgumentException("Terrain has not been selected!");
            }
            BattleSimulationController.battle = new Battle(armyOne, armyTwo, terrain);
            FXMLLoaderClass.goToBattleSimulation(actionEvent);
        }catch(IllegalArgumentException e){
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Error occured!");
            alert.setHeaderText("Something is missing!");
            alert.setContentText("Make sure both armies have names and at least one unit. ");
            alert.showAndWait();
        }


    }

    /**
     * Goes to the home page.
     * @param actionEvent
     * @throws IOException if FXML loading error occurs.
     */
    @FXML
    public void home(ActionEvent actionEvent) throws IOException {
        FXMLLoaderClass.goToHome(actionEvent);
    }
}
