package edu.ntnu.idatt2001.daniesky.wargamesproject.gui;

import edu.ntnu.idatt2001.daniesky.wargamesproject.gui.controllers.CreateArmiesController;
import edu.ntnu.idatt2001.daniesky.wargamesproject.gui.main.Application;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.stage.Stage;
import java.io.IOException;

/**
 * Class with static methods that loads new FXML files into GUI.
 */
public class FXMLLoaderClass {

    /**
     * Goes to create army page.
     * @param actionEvent
     * @throws IOException  if FXML loading error occurs.
     */
    @FXML
    public static void goToCreateArmies(ActionEvent actionEvent) throws IOException {
        FXMLLoader fxmlLoader = new FXMLLoader(Application.class.getClassLoader().getResource("createArmies.fxml"));
        Stage stage = (Stage)((Node)actionEvent.getSource()).getScene().getWindow();
        Scene scene = new Scene(fxmlLoader.load(), 1080, 720);

        stage.setTitle("Create armies");
        stage.setScene(scene);
        stage.show();
        CreateArmiesController.unitDataOne.clear();
        CreateArmiesController.unitDataTwo.clear();

    }

    /**
     * Goes to battle simulation page.
     * @param actionEvent
     * @throws IOException  if FXML loading error occurs.
     */
    @FXML
    public static void goToBattleSimulation(ActionEvent actionEvent) throws IOException {
        FXMLLoader fxmlLoader = new FXMLLoader(Application.class.getClassLoader().getResource("battleSimulation.fxml"));
        Stage stage = (Stage)((Node)actionEvent.getSource()).getScene().getWindow();
        Scene scene = new Scene(fxmlLoader.load(), 1080, 720);

        stage.setTitle("Battle Simulation");
        stage.setScene(scene);
        stage.show();
    }

    /**
     * Goes to edit army page.
     * @param actionEvent
     * @throws IOException  if FXML loading error occurs.
     */
    @FXML
    public static void goToEditArmy(ActionEvent actionEvent) throws IOException{
        FXMLLoader fxmlLoader = new FXMLLoader(Application.class.getClassLoader().getResource("editArmy.fxml"));
        Stage stage = (Stage)((Node)actionEvent.getSource()).getScene().getWindow();
        Scene scene = new Scene(fxmlLoader.load(), 1080, 720);

        stage.setTitle("Edit Army");
        stage.setScene(scene);
        stage.show();
    }

    /**
     * Goes to edit army page, used by the "my armies" page.
     * @param actionEvent
     * @throws IOException  if FXML loading error occurs.
     */
    @FXML
    public static void goToEditMyArmy(ActionEvent actionEvent) throws IOException{
        FXMLLoader fxmlLoader = new FXMLLoader(Application.class.getClassLoader().getResource("editMyArmy.fxml"));
        Stage stage = (Stage)((Node)actionEvent.getSource()).getScene().getWindow();
        Scene scene = new Scene(fxmlLoader.load(), 1080, 720);

        stage.setTitle("Edit Army");
        stage.setScene(scene);
        stage.show();
    }

    /**
     * Goes to the home page.
     * @param actionEvent
     * @throws IOException  if FXML loading error occurs.
     */
    @FXML
    public static void goToHome(ActionEvent actionEvent) throws IOException{
        FXMLLoader fxmlLoader = new FXMLLoader(Application.class.getClassLoader().getResource("startpage.fxml"));
        Stage stage = (Stage)((Node)actionEvent.getSource()).getScene().getWindow();
        Scene scene = new Scene(fxmlLoader.load(), 1080, 720);

        stage.setTitle("Wargames");
        stage.setScene(scene);
        stage.show();
    }

    /**
     * Loads to the my armies page.
     * @param actionEvent
     * @throws IOException
     */
    @FXML
    public static void goToMyArmies(ActionEvent actionEvent) throws IOException{
        FXMLLoader fxmlLoader = new FXMLLoader(Application.class.getClassLoader().getResource("myArmies.fxml"));
        Stage stage = (Stage)((Node)actionEvent.getSource()).getScene().getWindow();
        Scene scene = new Scene(fxmlLoader.load(), 1080, 720);

        stage.setTitle("My Armies");
        stage.setScene(scene);
        stage.show();
    }
    /**
     * Loads to the my new army page within the "my armies" page.
     * @param actionEvent
     * @throws IOException
     */
    @FXML
    public static void goToNewArmy(ActionEvent actionEvent) throws IOException{
        FXMLLoader fxmlLoader = new FXMLLoader(Application.class.getClassLoader().getResource("newArmy.fxml"));
        Stage stage = (Stage)((Node)actionEvent.getSource()).getScene().getWindow();
        Scene scene = new Scene(fxmlLoader.load(), 1080, 720);

        stage.setTitle("New Army");
        stage.setScene(scene);
        stage.show();
    }

}
