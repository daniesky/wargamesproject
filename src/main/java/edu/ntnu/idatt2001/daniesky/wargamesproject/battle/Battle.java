package edu.ntnu.idatt2001.daniesky.wargamesproject.battle;

import edu.ntnu.idatt2001.daniesky.wargamesproject.unit.Army;
import edu.ntnu.idatt2001.daniesky.wargamesproject.unit.Unit;
import java.util.Random;

/**
 * Class that describes a battle between two armies. The class has two armies, a variable for the winner and a variable
 * representing a Terrain type.
 */
public class Battle {
    private Army armyOne;
    private Army armyTwo;
    private Army winner;
    Terrain terrain;

    /**
     * Constructor that takes two armies as parameters.
     * @param armyOne first army within battle.
     * @param armyTwo second army within battle.
     * @param terrain The terrain where this battle will be simulated.
     */
    public Battle(Army armyOne, Army armyTwo, Terrain terrain) {
        if(armyOne.getUnits().size() == 0){
            throw new IllegalArgumentException("Army must have units");
        }
        this.armyOne = armyOne;
        if(armyTwo.getUnits().size() == 0){
            throw new IllegalArgumentException("Army must have units");
        }
        this.armyTwo = armyTwo;
        this.terrain = terrain;
    }
    /**
     * This method is the simulation of the battle. The while loop only continues when both armies have troops, and for
     * each iteration a random army is chosen to attack.
     * @return Returns a string containing all information processed within the simulation (attacks, deaths etc.)
     */
    public String simulate(boolean attackInfo, boolean deathInfo){
        StringBuilder stringBuilder = new StringBuilder();
        int dmg;
        while(armyOne.hasLivingUnits() && armyTwo.hasLivingUnits()){
            //Chooses which army strikes first on a 50/50 randomizer.
            Random rd = new Random();
            boolean armyOneStrikesFirst = rd.nextBoolean();
            if(armyOneStrikesFirst){
                Unit attacker = armyOne.getRandom();
                Unit victim = armyTwo.getRandom();
                dmg = attacker.attack(victim,this.terrain);
                if(attackInfo) {
                    stringBuilder.append(attackInfo(victim, attacker,dmg));
                }
                if(victim.isDead() && deathInfo){
                    stringBuilder.append(deathInfo(victim) + "    Remaining units in " + armyTwo.getName() +" : " + armyTwo.getLiving("all") + "\n");
                }
            }
            else{
                Unit attacker = armyTwo.getRandom();
                Unit victim = armyOne.getRandom();
                dmg = attacker.attack(victim,this.terrain);
                if(attackInfo) {
                    stringBuilder.append(attackInfo(victim, attacker, dmg));
                }
                if(victim.isDead() && deathInfo){
                    stringBuilder.append(deathInfo(victim) + "    Remaining units in " + armyOne.getName() +" : " + armyOne.getLiving("all") + "\n");
                }
            }
        }
        if(!armyOne.hasLivingUnits()){
            stringBuilder.append(winInfo(armyTwo));
            winner = armyTwo;
        }
        if(!armyTwo.hasLivingUnits()){
            stringBuilder.append(winInfo(armyOne));
            winner = armyOne;
        }
        return stringBuilder.toString();
    }

    /**
     * @param attacker unit which is attacking.
     * @param victim unit being attacked.
     * @return Returns information in a String about who attacks who.
     */
    private String attackInfo(Unit victim, Unit attacker, int dmg){
        return attacker.getName() + " attacks " + victim.getName() +" for " + dmg + " damage! "
                + victim.getName() + " now has " + victim.getHealth() + " health. \n";

    }

    /**
     * @param unit unit that has died.
     * @return Returns information about a unit that has recently died.
     */
    private String deathInfo(Unit unit){
        if(!unit.isDead()){
            throw new IllegalArgumentException("Unit is not dead");
        }
        return "----" + unit.getName() + " has died. ";
    }

    /**
     * @param victoriousArmy army that has won the battle.
     * @return Returns a string describing a victory for the army given in the parameter.
     */
    public static String winInfo(Army victoriousArmy){
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(victoriousArmy.getName() + " has won the battle!");
        stringBuilder.append("\nThe army had " + victoriousArmy.getLiving("all") + " remaining units!");
        return stringBuilder.toString();
    }


    public Army getArmyOne() {
        return armyOne;
    }
    public Army getArmyTwo() {
        return armyTwo;
    }
    public void setArmyOne(Army armyOne) {
        this.armyOne = armyOne;
    }
    public void setArmyTwo(Army armyTwo) {
        this.armyTwo = armyTwo;
    }
    public Army getWinner() {
        return winner;
    }
    public Terrain getTerrain() {
        return terrain;
    }
    public void setTerrain(Terrain terrain) {
        this.terrain = terrain;
    }

    @Override
    public String toString() {
        return "Battle{" +
                "armyOne=" + armyOne +
                ", armyTwo=" + armyTwo +
                ", terrain=" + terrain +
                '}';
    }
}
