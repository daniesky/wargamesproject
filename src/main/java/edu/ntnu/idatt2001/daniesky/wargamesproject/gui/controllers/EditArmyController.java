package edu.ntnu.idatt2001.daniesky.wargamesproject.gui.controllers;

import edu.ntnu.idatt2001.daniesky.wargamesproject.fileHandling.FileHandler;
import edu.ntnu.idatt2001.daniesky.wargamesproject.gui.FXMLLoaderClass;
import edu.ntnu.idatt2001.daniesky.wargamesproject.unit.Army;
import edu.ntnu.idatt2001.daniesky.wargamesproject.unit.Unit;
import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.AnchorPane;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import javafx.util.Duration;
import org.controlsfx.control.PopOver;
import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.Optional;
import java.util.ResourceBundle;

/**
 * Controller for the editArmy FMXL file. This controller is called upon within the Battle Simulation page.
 */
public class EditArmyController implements Initializable {

    //static variable holding the army being edited.
    public static Army army;

    @FXML private TextField nameLabel;
    @FXML private TableView<Unit> armyTable;
    @FXML private TableColumn unitColumn;
    @FXML private TableColumn nameColumn;
    @FXML private TableColumn hpColumn;
    @FXML private TableColumn atColumn;
    @FXML private TableColumn arColumn;
    @FXML private Button addUnits;
    @FXML private Label saveFeedBack;


    //List with units that is interacted with by other controllers.
    public static ObservableList<Unit> unitData = FXCollections.observableArrayList();

    /**
     * Initializes table and name label with army information.
     * @param url
     * @param resourceBundle
     */
    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        nameLabel.setText(army.getName());
        nameColumn.setCellValueFactory(new PropertyValueFactory<>("name"));
        hpColumn.setCellValueFactory(new PropertyValueFactory<>("health"));
        atColumn.setCellValueFactory(new PropertyValueFactory<>("attack"));
        arColumn.setCellValueFactory(new PropertyValueFactory<>("armor"));
        unitColumn.setCellValueFactory(new PropertyValueFactory<>("unitName"));
        unitData.clear();
        unitData.addAll(army.getUnits());
        refreshTable();
    }

    /**
     * Refreshes table when updates happen.
     */
    public void refreshTable(){
        armyTable.setItems(unitData);
    }

    /**
     * Shows pop up for adding units.
     * @param actionEvent
     * @throws IOException  if FXML loading error occurs.
     */
    @FXML
    public void addUnitsPopUp(ActionEvent actionEvent) throws IOException {
        AddUnitsPopUpController.army = 3;
        AnchorPane content = new AnchorPane();
        PopOver popOver = new PopOver();
        URL url = getClass().getClassLoader().getResource("addUnitsPopUp.fxml");
        content.getChildren().setAll((Node) FXMLLoader.load(url));
        popOver.setContentNode(content);
        popOver.show(addUnits);
        refreshTable();
    }

    /**
     * Imports a new army from file.
     * @param actionEvent
     */
    @FXML
    public void importArmy(ActionEvent actionEvent){
        Stage stage = new Stage();
        FileChooser fileChooser = new FileChooser();
        fileChooser.setTitle("Open Army File");
        File f = fileChooser.showOpenDialog(stage);
        FileHandler fileHandler = new FileHandler("Armies/");
        try {
            Army army = fileHandler.readArmyFile(f);
            if(army != null) {
                this.army = army;
                unitData.clear();
                unitData.addAll(army.getUnits());
                nameLabel.setText(army.getName());
                refreshTable();
            }
        }catch(IllegalArgumentException e){
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Error occured");
            alert.setHeaderText("An error occured while reading the file");
            alert.setContentText("Something is wrong with uploaded file. It might be corrupt or in the wrong format. ");
            alert.showAndWait();
        }
        //Catches exception thrown for null files.
        catch(NullPointerException e){

        }
    }

    /**
     * Deletes a unit from table and army.
     * @param actionEvent
     */
    @FXML
    public void delete(ActionEvent actionEvent){
        Unit unit = armyTable.getSelectionModel().getSelectedItem();
        army.remove(unit);
        unitData.remove(unit);
        refreshTable();
    }

    /**
     * Done button pressed, army information is saved and you return to Battle simulation.
     * @param actionEvent
     * @throws IOException  if FXML loading error occurs.
     */
    @FXML
    public void doneEdit(ActionEvent actionEvent) throws IOException {
        army.setName(nameLabel.getText());
        try {
            if (BattleSimulationController.battle.getArmyOne() == null) {
                BattleSimulationController.battle.setArmyOne(new Army(army));
                FXMLLoaderClass.goToBattleSimulation(actionEvent);
            } else if (BattleSimulationController.battle.getArmyTwo() == null) {
                BattleSimulationController.battle.setArmyTwo(new Army(army));
                FXMLLoaderClass.goToBattleSimulation(actionEvent);
            }
        }catch(IllegalArgumentException e){
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Error occured!");
            alert.setHeaderText("Something is missing!");
            alert.setContentText("Make sure army has filled in name and atleast one unit. ");
            alert.showAndWait();
        }
    }

    /**
     * Revives all units in the table.
     * @param actionEvent
     */
    @FXML
    public void revive(ActionEvent actionEvent){
        army.reviveAll();
        unitData.clear();
        unitData.addAll(army.getUnits());
        refreshTable();
    }
    /**
     * Method that displays a text label that says successful save.
     */
    @FXML
    private void successfulSave(){
        Timeline t = new Timeline(
                new KeyFrame(Duration.ZERO, new EventHandler<ActionEvent>() {
                    @Override
                    public void handle(ActionEvent actionEvent) {
                        saveFeedBack.setText("Successful save!");
                    }
                }),
                new KeyFrame(Duration.seconds(2), new EventHandler<ActionEvent>() {
                    @Override
                    public void handle(ActionEvent actionEvent) {
                        saveFeedBack.setText("");
                    }
                }));
        t.play();
    }

    /**
     * Saves the army to the current file, can be within my armies.
     * @param actionEvent
     */
    @FXML
    public void saveToCurrent(ActionEvent actionEvent){
        if(army.getSize() == 0 || nameLabel.getText().isBlank()){
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Could not save");
            alert.setHeaderText("Something is missing");
            alert.setContentText("Make sure name is filled out and has atleast one unit. ");
            alert.showAndWait();
            throw new IllegalArgumentException("Army is missing name and/or units");
        }
        army.setName(nameLabel.getText());
        if(army.getPath() == null){
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("File not found");
            alert.setHeaderText("This army is not imported.");
            alert.setContentText("Army is not imported, try saving to new file or my armies.");
            alert.showAndWait();
        }
        else{
            try {
                FileHandler fileHandler = new FileHandler("Armies/");
                fileHandler.updateFile(army);
                successfulSave();
            }
            catch(IllegalArgumentException e){
                Alert alert = new Alert(Alert.AlertType.ERROR);
                alert.setTitle("Fail save");
                alert.setHeaderText("Could not save army to current file.");
                alert.setContentText("Could not write to the specified file, file might be wrong format or corrupt.");
                alert.showAndWait();
            }
            catch(NullPointerException e){
                Alert alert = new Alert(Alert.AlertType.ERROR);
                alert.setTitle("Fail save");
                alert.setHeaderText("Could not save army to current file.");
                alert.setContentText("Could not find file. File might have been " +
                        "deleted or moved from the directory it was imported from. ");
                alert.showAndWait();
            }
        }
    }

    /**
     * Saves the file to a user specified directory.
     * @param actionEvent
     */
    @FXML
    public void saveToNewFile(ActionEvent actionEvent){
        if(army.getSize() == 0 || nameLabel.getText().isBlank()){
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Could not save");
            alert.setHeaderText("Something is missing");
            alert.setContentText("Make sure name is filled out and has atleast one unit. ");
            alert.showAndWait();
            throw new IllegalArgumentException("Army is missing name and/or units");
        }
        Stage stage = new Stage();
        FileChooser fileChooser = new FileChooser();
        FileChooser.ExtensionFilter extFilter = new FileChooser.ExtensionFilter("CSV files (*.csv)", "*.csv");
        fileChooser.getExtensionFilters().add(extFilter);
        fileChooser.setTitle("Save army file");
        fileChooser.setInitialFileName(army.getName().replaceAll(" ",""));
        File file = fileChooser.showSaveDialog(stage);
        try {
            file.createNewFile();
            FileHandler fileHandler = new FileHandler("Armies/");
            army.setImportPath(file.getAbsolutePath());
            fileHandler.writeToArmyFile(file,army);
            successfulSave();
        }catch(IOException e){
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Fail save");
            alert.setHeaderText("Could not save army to new file.");
            alert.setContentText("Something went wrong when creating file.");
            alert.showAndWait();
        }
    }

    /**
     * Saves the army to my armies.
     */
    @FXML
    public void saveToMyArmies(){
        if(army.getSize() == 0 || nameLabel.getText().isBlank()){
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Could not save");
            alert.setHeaderText("Something is missing");
            alert.setContentText("Make sure name is filled out and has atleast one unit. ");
            alert.showAndWait();
            throw new IllegalArgumentException("Army is missing name and/or units");
        }
        army.setName(nameLabel.getText());
        FileHandler fileHandler = new FileHandler("Armies/");
        try {
            fileHandler.createArmyFile(army);
            successfulSave();
        }catch(IllegalArgumentException e){
            Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
            alert.setTitle("Fail save");
            alert.setHeaderText("Army file already exists!");
            alert.setContentText("An army with this name already exists in your armies!" +
                    "Would you like to overwrite this army with your current army?");
            Optional<ButtonType> option = alert.showAndWait();

            if(option.get() == ButtonType.OK){
                fileHandler.updateFile(army);
                successfulSave();
            }
        }

    }


}
