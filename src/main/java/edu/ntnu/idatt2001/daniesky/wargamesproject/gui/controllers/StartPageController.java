package edu.ntnu.idatt2001.daniesky.wargamesproject.gui.controllers;

import edu.ntnu.idatt2001.daniesky.wargamesproject.gui.FXMLLoaderClass;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import java.io.IOException;

/**
 * Controller for the home/start page.
 */
public class StartPageController {

    public Button startButton;

    /**
     * Goes to create armies.
     * @param actionEvent
     * @throws IOException  if FXML loading error occurs.
     */
    @FXML
    public void startButtonPressed(ActionEvent actionEvent) throws IOException {
        FXMLLoaderClass.goToCreateArmies(actionEvent);
    }

    /**
     * Goes to the my armies page.
     * @param actionEvent
     * @throws IOException
     */
    @FXML
    public void myArmies(ActionEvent actionEvent) throws IOException{
        FXMLLoaderClass.goToMyArmies(actionEvent);
    }
}
