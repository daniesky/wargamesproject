package edu.ntnu.idatt2001.daniesky.wargamesproject.unit;

import edu.ntnu.idatt2001.daniesky.wargamesproject.battle.Terrain;

/**
 * This subclass described a ranged unit.
 */
public class RangedUnit extends Unit{

    /**
     * Constructor with all four parameters.
     * @param name
     * @param health
     * @param attack
     * @param armor
     */


    public RangedUnit(String name, int health, int attack, int armor) {
        super(name, health, attack, armor);
    }

    /**
     * Constructor with attack and armor predefined.
     * @param name
     * @param health
     */
    public RangedUnit(String name, int health) {
        super(name, health, 15, 8);
    }

    @Override
    /**
     * Returns the attack stat. Standard attack stat is 3, but in Hill it has an advantage and in the forest it has
     * a disadvantage.
     */
    public int getAttackBonus(Terrain terrain) {
        switch (terrain){
            case HILL -> {
                return 5;
            }
            case FOREST -> {
                return 1;
            }
        }
        return 3;
    }

    @Override
    public int getResistBonus(Terrain terrain) {
        /**
         * Returns a larger defense bonus for the first two attacks.
         */
        if(this.attacksReceived == 0){
            return 6;
        }
        else if(this.attacksReceived == 1){
            return 4;
        }
        else{
            return 2;
        }
    }

    /**
     * setHealth method for this class also adds to the attacksReceived when the health is changed as the unit o
     * only loses health when attacked.
     * @param health
     */
    @Override
    public void setHealth(int health) {
        if(health < 0){
            health = 0;
        }
        super.setHealth(health);
    }
}
