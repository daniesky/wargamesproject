package edu.ntnu.idatt2001.daniesky.wargamesproject.gui.controllers;

import edu.ntnu.idatt2001.daniesky.wargamesproject.battle.Battle;
import edu.ntnu.idatt2001.daniesky.wargamesproject.battle.Terrain;
import edu.ntnu.idatt2001.daniesky.wargamesproject.gui.FXMLLoaderClass;
import edu.ntnu.idatt2001.daniesky.wargamesproject.gui.models.ArmySummaryModel;
import edu.ntnu.idatt2001.daniesky.wargamesproject.unit.Army;
import edu.ntnu.idatt2001.daniesky.wargamesproject.unit.Unit;
import javafx.application.Platform;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import java.io.IOException;
import java.net.URL;
import java.util.List;
import java.util.ResourceBundle;
import java.util.Scanner;

/**
 * Controller for the Battle Simulation FXML file.
 */
public class BattleSimulationController implements Initializable {

    public static Battle battle;
    private ObservableList<Unit> displayUnits = FXCollections.observableArrayList();
    private ObservableList<ArmySummaryModel> listOne = FXCollections.observableArrayList();
    private ObservableList<ArmySummaryModel> listTwo = FXCollections.observableArrayList();

    @FXML private TableView<ArmySummaryModel> tableOne;
    @FXML private TableColumn<ArmySummaryModel , SimpleStringProperty> unitOne;
    @FXML private TableColumn<ArmySummaryModel, SimpleIntegerProperty> amountOne;
    @FXML private TableColumn<ArmySummaryModel, SimpleIntegerProperty> deathsOne;
    @FXML private TableView<ArmySummaryModel> tableTwo;
    @FXML private TableColumn<ArmySummaryModel, SimpleStringProperty> unitTwo;
    @FXML private TableColumn<ArmySummaryModel,SimpleIntegerProperty> amountTwo;
    @FXML private TableColumn<ArmySummaryModel,SimpleIntegerProperty> deathsTwo;
    @FXML private Label nameOne;
    @FXML private Label nameTwo;
    @FXML private ComboBox importPathOne;
    @FXML private ComboBox importPathTwo;
    @FXML private CheckBox forestBox;
    @FXML private CheckBox hillBox;
    @FXML private CheckBox plainBox;
    @FXML private TableView<Unit> armyTable;
    @FXML private TableColumn unitColumn;
    @FXML private TableColumn nameColumn;
    @FXML private TableColumn hpColumn;
    @FXML private TableColumn atColumn;
    @FXML private TableColumn arColumn;
    @FXML private TextArea textBox;
    @FXML private CheckBox attackBox;
    @FXML private CheckBox deathBox;
    @FXML private CheckBox normalBox;
    @FXML private CheckBox slowBox;
    @FXML private CheckBox rapidBox;
    @FXML private CheckBox instantBox;
    @FXML private Button reset;
    @FXML private Button simulate;
    @FXML private Button editOne;
    @FXML private Button editTwo;
    @FXML private Button viewUnitsOne;
    @FXML private Button viewUnitsTwo;
    @FXML private Button cancel;

    private boolean cancelRun;

    /*
    Method that initializes the GUI page: Filling tables and setting information paths etc.
     */
    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        //Initalize tables, so that they can retrieve correct information for Unit class.
        unitOne.setCellValueFactory(new PropertyValueFactory<>("unitType"));
        amountOne.setCellValueFactory(new PropertyValueFactory<>("amount"));
        deathsOne.setCellValueFactory(new PropertyValueFactory<>("deaths"));
        unitTwo.setCellValueFactory(new PropertyValueFactory<>("unitType"));
        amountTwo.setCellValueFactory(new PropertyValueFactory<>("amount"));
        deathsTwo.setCellValueFactory(new PropertyValueFactory<>("deaths"));

        nameColumn.setCellValueFactory(new PropertyValueFactory<>("name"));
        hpColumn.setCellValueFactory(new PropertyValueFactory<>("health"));
        atColumn.setCellValueFactory(new PropertyValueFactory<>("attack"));
        arColumn.setCellValueFactory(new PropertyValueFactory<>("armor"));
        unitColumn.setCellValueFactory(new PropertyValueFactory<>("unitName"));
        //Hides the optional army table from start.
        armyTable.setVisible(false);

        //Disables horizontal scrolling in textBox.
        textBox.setWrapText(true);

        Terrain t = battle.getTerrain();
        updateImportPaths();
        refreshTable();
        switch(t){
            case FOREST -> {
                forestBox.setSelected(true);
                break;
            }
            case HILL -> {
                hillBox.setSelected(true);
                break;
            }
            case PLAINS -> {
                plainBox.setSelected(true);
                break;
            }

        }
        cancel.setVisible(false);
    }

    /**
     * Method that refreshes the tables.
     */
    @FXML
    public void refreshTable(){
        listOne.clear();
        fillTable(battle.getArmyOne(),listOne);
        tableOne.setItems(listOne);
        listTwo.clear();
        fillTable(battle.getArmyTwo(), listTwo);
        tableTwo.setItems(listTwo);
        nameOne.setText(battle.getArmyOne().getName());
        nameTwo.setText(battle.getArmyTwo().getName());
    }

    /**
     * Fills/updates the tables when changes are made.
     * @param army army that table is being filled with.
     * @param list List that is connected to table which is being filled.
     */
    private void fillTable(Army army, ObservableList<ArmySummaryModel> list){
        List<Unit> unitList = army.getInfantryUnits();
        int deathToll;
        ArmySummaryModel armySummaryModel;
        if(!unitList.isEmpty()){
            deathToll = army.getUnitDeathToll("InfantryUnit");
            armySummaryModel = new ArmySummaryModel("InfantryUnit",unitList.size(),deathToll);
            list.add(armySummaryModel);
        }
        unitList.clear();

        unitList = army.getRangedUnits();
        if(!unitList.isEmpty()){
            deathToll = army.getUnitDeathToll("RangedUnit");
            armySummaryModel = new ArmySummaryModel("RangedUnit",unitList.size(),deathToll);
            list.add(armySummaryModel);
        }
        unitList.clear();

        unitList = army.getCavalryUnits();
        if(!unitList.isEmpty()){
            deathToll = army.getUnitDeathToll("CavalryUnit");
            armySummaryModel = new ArmySummaryModel("CavalryUnit",unitList.size(),deathToll);
            list.add(armySummaryModel);
        }
        unitList.clear();

        unitList = army.getCommanderUnits();
        if(!unitList.isEmpty()){
            deathToll = army.getUnitDeathToll("CommanderUnit");
            armySummaryModel = new ArmySummaryModel("CommanderUnit",unitList.size(),deathToll);
            list.add(armySummaryModel);
        }
        unitList.clear();
    }

    /**
     * Updates the comboboxes that display the import paths.
     */
    @FXML
    public void updateImportPaths(){
        String path = battle.getArmyOne().getPath();
        if(path == null){
            importPathOne.getItems().add("Not imported");
        }
        else {
            importPathOne.getItems().add(battle.getArmyOne().getPath());
        }
        path = battle.getArmyTwo().getPath();
        if(path == null){
            importPathTwo.getItems().add("Not imported");
        }
        else {
            importPathTwo.getItems().add(battle.getArmyTwo().getPath());
        }
    }

    /**
     * Goes to the edit army page with the first army.
     * @param actionEvent
     * @throws IOException if FXML loading error occurs.
     */
    @FXML
    public void editFirstArmy(ActionEvent actionEvent) throws IOException {
        EditArmyController.army = battle.getArmyOne();
        battle.setArmyOne(null);
        FXMLLoaderClass.goToEditArmy(actionEvent);
    }

    /**
     * Goes to the edit army page with the second army.
     * @param actionEvent
     * @throws IOException  if FXML loading error occurs.
     */
    @FXML
    public void editSecondArmy(ActionEvent actionEvent) throws IOException {
            EditArmyController.army = battle.getArmyTwo();
            battle.setArmyTwo(null);
        FXMLLoaderClass.goToEditArmy(actionEvent);
    }

    /**
     * Handles the forest box. If forest is selected, all other boxes are deselected.
     * @param actionEvent
     */
    @FXML
    public void handleForestBox(ActionEvent actionEvent){
        forestBox.setSelected(true);
        hillBox.setSelected(false);
        plainBox.setSelected(false);
        battle.setTerrain(Terrain.FOREST);
    }
    /**
     * Handles the hill box. If forest is selected, all other boxes are deselected.
     * @param actionEvent
     */
    @FXML
    public void handleHillBox(ActionEvent actionEvent){
        forestBox.setSelected(false);
        hillBox.setSelected(true);
        plainBox.setSelected(false);
        battle.setTerrain(Terrain.HILL);
    }
    /**
     * Handles the plains box. If forest is selected, all other boxes are deselected.
     * @param actionEvent
     */
    @FXML
    public void handlePlainBox(ActionEvent actionEvent){
        forestBox.setSelected(false);
        hillBox.setSelected(false);
        plainBox.setSelected(true);
        battle.setTerrain(Terrain.PLAINS);
    }

    /**
     * Fills the hidden army table with army 1 units and displays it.
     * @param actionEvent
     */
    @FXML
    public void showUnitsOne(ActionEvent actionEvent){
        displayUnits.clear();
        displayUnits.addAll(battle.getArmyOne().getUnits());
        armyTable.setItems(displayUnits);
        if(armyTable.isVisible()){
            armyTable.setVisible(false);
        }
        else{
            armyTable.setVisible(true);
        }
    }
    /**
     * Fills the hidden army table with army 2 units and displays it.
     * @param actionEvent
     */
    @FXML
    public void showUnitsTwo(ActionEvent actionEvent){
        displayUnits.clear();
        displayUnits.addAll(battle.getArmyTwo().getUnits());
        armyTable.setItems(displayUnits);
        if(armyTable.isVisible()){
            armyTable.setVisible(false);
        }
        else{
            armyTable.setVisible(true);
        }
    }

    /**
     * Clears the text box and revives all units.
     * @param actionEvent
     */
    @FXML
    public void reset(ActionEvent actionEvent){
        textBox.clear();
        battle.getArmyOne().reviveAll();
        battle.getArmyTwo().reviveAll();
        refreshTable();
        cancel.setVisible(false);
    }

    /**
     * Goes to the home page.
     * @param actionEvent
     * @throws IOException  if FXML loading error occurs.
     */
    @FXML
    public void home(ActionEvent actionEvent) throws IOException {
        battle = null;
        listOne.clear();
        listTwo.clear();
        CreateArmiesController.unitDataOne.clear();
        CreateArmiesController.unitDataTwo.clear();
        FXMLLoaderClass.goToHome(actionEvent);
    }

    /**
     * Handles the speed boxes. If normal is selected, all others are deselected.
     * @param actionEvent
     */
    @FXML
    public void handleNormalBox(ActionEvent actionEvent){
        slowBox.setSelected(false);
        normalBox.setSelected(true);
        rapidBox.setSelected(false);
        instantBox.setSelected(false);
    }
    /**
     * Handles the speed boxes. If rapid is selected, all others are deselected.
     * @param actionEvent
     */
    @FXML
    public void handleRapidBox(ActionEvent actionEvent){
        slowBox.setSelected(false);
        normalBox.setSelected(false);
        rapidBox.setSelected(true);
        instantBox.setSelected(false);
    }
    /**
     * Handles the speed boxes. If instant is selected, all others are deselected.
     * @param actionEvent
     */
    @FXML
    public void handleInstantBox(ActionEvent actionEvent){
        slowBox.setSelected(false);
        normalBox.setSelected(false);
        rapidBox.setSelected(false);
        instantBox.setSelected(true);
    }
    /**
     * Handles the speed boxes. If slow is selected, all others are deselected.
     * @param actionEvent
     */
    @FXML
    public void handleSlowBox(ActionEvent actionEvent){
        slowBox.setSelected(true);
        normalBox.setSelected(false);
        rapidBox.setSelected(false);
        instantBox.setSelected(false);
    }

    /**
     * Returns the selected speed from the display speed menu.
     * @return
     */
    private int informationSpeed(){
       if(slowBox.isSelected()){
           return 1000;
       }
       else if(normalBox.isSelected()){
           return 250;
       }
       else if(rapidBox.isSelected()){
           return 50;
       }
       else if(instantBox.isSelected()){
           return 0;
       }
       else{
           textBox.setText("Choose speed in order to simulate!");
           cancel.setVisible(false);
           throw new IllegalArgumentException("Speed must be selected");
       }
    }

    /**
     * Sets button disabled or not, used to disable buttons when running simulator.
     * @param a boolean if buttons are disabled or not.
     */
    private void setButtons(boolean a){
        reset.setDisable(a);
        simulate.setDisable(a);
        editOne.setDisable(a);
        editTwo.setDisable(a);
        viewUnitsOne.setDisable(a);
        viewUnitsTwo.setDisable(a);
        forestBox.setDisable(a);
        hillBox.setDisable(a);
        plainBox.setDisable(a);
        slowBox.setDisable(a);
        normalBox.setDisable(a);
        rapidBox.setDisable(a);
        instantBox.setDisable(a);
        attackBox.setDisable(a);
        deathBox.setDisable(a);
    }

    /**
     * If cancel button is clicked, cancelRun is set true, and whileloop in simulate method will stop.
     * @param actionEvent
     */
    @FXML
    public void cancelSimulation(ActionEvent actionEvent){
        this.cancelRun = true;
    }

    /**
     * Simulates the battle. Method creates a new thread with internal runnable in order to update textBox
     * part by part within while loop.
     * @param actionEvent
     */
    @FXML
    public void simulate(ActionEvent actionEvent){
        cancel.setVisible(true);
        //Gets the sleep time.
        int sleep = this.informationSpeed();

        setButtons(true);

        //Creates the new thread to run while loop on.
        Thread thread = new Thread(new Runnable() {
            @Override
            public void run() {
                Runnable updater = new Runnable() {
                    @Override
                    public void run() {
                    }
                };
                    //Gets all simulation information from simulate method. Takes input from display information boxes.
                    String result = battle.simulate(attackBox.isSelected(),deathBox.isSelected());
                    Scanner scanner = new Scanner(result);
                    String print = "";
                    while(scanner.hasNextLine() && !cancelRun){
                        try {
                            //Sleep == 0 has to be done in another way in order to prevent crashes.
                            if(sleep != 0){
                                print = scanner.nextLine() + "\n";
                                textBox.appendText(print);
                                Thread.sleep(sleep);
                            }
                            else{
                                print = print + scanner.nextLine() + "\n";
                            }
                        }
                            catch(Exception e) {
                                cancel.setVisible(false);
                                textBox.setText(e.toString());
                            }
                        //Runs the runnable created earlier.
                        Platform.runLater(updater);
                        }
                    //Sets box instantly.
                    if(sleep == 0){
                        textBox.setText(print);
                    }
                refreshTable();
                    cancel.setVisible(false);
                    setButtons(false);
                }

            });
        //This prevents JVM shutdown.
        thread.setDaemon(true);
        thread.start();
        cancelRun = false;
        }
}
