package edu.ntnu.idatt2001.daniesky.wargamesproject.unit;

import edu.ntnu.idatt2001.daniesky.wargamesproject.battle.Terrain;

/**
 * Subclass of the unit class that creates a cavalry unit.
 */
public class CavalryUnit extends Unit{
    private boolean hasAttacked;

    /**
     * Constructor with the option for attack and armor stats even though they are predefined for the unit.
     * @param name
     * @param health
     * @param attack
     * @param armor
     */
    public CavalryUnit(String name, int health, int attack, int armor) {
        super(name, health, attack, armor);
        this.hasAttacked = false;
    }

    /**
     * Constructor for the cavalry unit only needs the name and the health of the soldier.
     * The attack and armor stats are defined for the type of unit.
     * @param name
     * @param health
     */
    public CavalryUnit(String name, int health) {
        super(name, health, 20,12);
        this.hasAttacked = false;
    }

    @Override
    /**
     * Returns a bigger attack bonus if the unit has attacked.
     */
    public int getAttackBonus(Terrain terrain) {
        int totalbonus = 0;
        if(!this.hasAttacked){
            totalbonus += 4+2;
        }
        else{
            totalbonus += 2;
        }
        if(terrain == Terrain.PLAINS){
            totalbonus += 2;
        }
        return totalbonus;

    }

    @Override
    /**
     * Return 1 armor bonus.
     */
    public int getResistBonus(Terrain terrain) {
        if(terrain == Terrain.FOREST){
            return 0;
        }
        else {
            return 1;
        }
    }



    @Override
    /**
     * Exact same method as for the superclass only that it defines hasAttacked as true.
     */
    public int attack(Unit opponent,Terrain terrain) {
        int dmg = super.attack(opponent,terrain);
        this.hasAttacked = true;
        return dmg;
    }
}
