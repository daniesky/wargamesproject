package edu.ntnu.idatt2001.daniesky.wargamesproject.gui.controllers;

import edu.ntnu.idatt2001.daniesky.wargamesproject.fileHandling.FileHandler;
import edu.ntnu.idatt2001.daniesky.wargamesproject.gui.FXMLLoaderClass;
import edu.ntnu.idatt2001.daniesky.wargamesproject.unit.Army;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Optional;
import java.util.ResourceBundle;

/**
 * Controller that handles the "my armies" page in the GUI. This page allows user to edit, delete and create new armies.
 */
public class MyArmiesController implements Initializable{

    @FXML private TableView<Army> armies;
    @FXML private TableColumn<Army, String> armyName;
    @FXML private TableColumn<Army, Integer> armySize;
    @FXML private TableColumn<Army, String> armyPath;


    //Static variable that is needed to upload data to this controller from other controllers.
    public static ObservableList<Army> unitData = FXCollections.observableArrayList();

    /**
     * Initializes the table with property value factories that retrieve name size and the path of armies.
     * @param url
     * @param resourceBundle
     */
    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        armyName.setCellValueFactory(new PropertyValueFactory<>("name"));
        armySize.setCellValueFactory(new PropertyValueFactory<>("size"));
        armyPath.setCellValueFactory(new PropertyValueFactory<>("path"));
        try {
            retrieveArmies();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Takes the static unitData and adds all items to the table. This is done after new armies are added/deleted or edited.
     */
    private void refreshTable(){
        armies.setItems(unitData);
    }

    /**
     * Private method that retrieves army from the Armies file folder, creates the armies and adds them to the table.
     * @throws IOException  if FXML loading error occurs.
     */
    private void retrieveArmies() throws IOException {
        unitData.clear();
        FileHandler f = new FileHandler("Armies/");
        ArrayList<Army> armies = new ArrayList<>();
        Files.walk(Paths.get("Armies"))
                .filter(Files::isRegularFile)
                .forEach(Path -> armies.add(f.readArmyFile(new File(Path.toString()))));
        unitData.addAll(armies);
        refreshTable();

    }

    /**
     * Deletes an army from the display table and the armies file folder. Asks the user for confirmation before deleting.
     * @param actionEvent
     */
    @FXML
    public void delete(ActionEvent actionEvent){
        Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
        alert.setTitle("Confirm delete");
        alert.setHeaderText("Confirm delete of army");
        alert.setContentText("Are you sure you want to delete this army? This will also delete the file with army data.");
        Optional<ButtonType> result = alert.showAndWait();
        if(result.get() == ButtonType.OK){
            unitData.remove(armies.getSelectionModel().getSelectedItem());
            refreshTable();
            FileHandler fileHandler = new FileHandler("Armies/");
            fileHandler.deleteFile(armies.getSelectionModel().getSelectedItem());
        }
    }

    /**
     * Calls the FXMLLoaderClass goToHome method; loads home page.
     * @param actionEvent
     * @throws IOException  if FXML loading error occurs.
     */
    @FXML
    public void goToHome(ActionEvent actionEvent) throws  IOException{
        FXMLLoaderClass.goToHome(actionEvent);
    }

    /**
     * Calls FXMLLoaderClass in order to load edit army fxml file.
     * @param actionEvent
     * @throws IOException  if FXML loading error occurs.
     */
    @FXML
    public void editArmy(ActionEvent actionEvent) throws  IOException{
        Army a = armies.getSelectionModel().getSelectedItem();
        EditMyArmyController.army = a;
        FXMLLoaderClass.goToEditMyArmy(actionEvent);
    }

    /**
     * Calls FXMLLoaderClass in order to load new army fxml file.
     * @param actionEvent
     * @throws IOException  if FXML loading error occurs.
     */
    @FXML
    public void newArmy(ActionEvent actionEvent) throws  IOException{
        FXMLLoaderClass.goToNewArmy(actionEvent);
    }

}
