package edu.ntnu.idatt2001.daniesky.wargamesproject.battle;

/**
 * Enum class that represents the terrain within the simulator. The units act differently on the different terrains.
 */
public enum Terrain {
    HILL,
    PLAINS,
    FOREST
}
