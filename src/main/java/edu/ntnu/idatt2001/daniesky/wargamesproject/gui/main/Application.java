package edu.ntnu.idatt2001.daniesky.wargamesproject.gui.main;

import edu.ntnu.idatt2001.daniesky.wargamesproject.fileHandling.FileHandler;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import java.net.URL;

/**
 * Application class used to run the first scene of the GUI, and start the entire program.
 */
public class Application extends javafx.application.Application{
    /**
     * When app is started, startpage is loaded and displayed.
     * @param stage starting stage.
     * @throws Exception exception if any error occurs when loading first stage with fxml file.
     */
    @Override
    public void start(Stage stage) throws Exception {
        URL url = getClass().getClassLoader().getResource("startpage.fxml");
        Parent root = FXMLLoader.load(url);
        Scene scene = new Scene(root);
        stage.setTitle("Wargames");
        stage.setScene(scene);
        stage.show();
        FileHandler.controlDirectory();

    }

    /**
     * Main class launches application.
     * @param args args
     */
    public static void main(String[] args) {
        launch(args);
    }
}
