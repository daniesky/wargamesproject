package edu.ntnu.idatt2001.daniesky.wargamesproject.unit;

import edu.ntnu.idatt2001.daniesky.wargamesproject.battle.Terrain;

/**
 * Abstract superclass representing a general unit. Is defined by unit name, health, attack and armor.
 *
 */
public abstract class Unit {
    protected String name;
    protected int totalHealth;
    protected int health;
    protected int attack;
    protected int armor;
    protected int attacksReceived;

    /**
     * Constructor for the superclass Unit. Thows exception if you try creating a trooper with 0 or less health.
     * @param name
     * @param health
     * @param attack
     * @param armor
     */
    public Unit(String name, int health, int attack, int armor) {
        if(name.isBlank()){
            throw new IllegalArgumentException("Name can not be blank.");
        }
        this.name = name;
        if(health <= 0){
            throw new IllegalArgumentException("Health must be above 0.");
        }
        this.health = health;
        this.totalHealth = health;

        if(attack < 0){
            throw new IllegalArgumentException("Attack stat must be 0 or above.");
        }
        this.attack = attack;
        if(armor < 0){
            throw new IllegalArgumentException("Armor stat must be 0 or above.");
        }
        this.armor = armor;
        this.attacksReceived = 0;
    }

    public String getName() {
        return name;
    }
    public int getHealth() {
        return health;
    }
    public int getAttack() {
        return attack;
    }
    public int getArmor() {
        return armor;
    }
    abstract int getAttackBonus(Terrain terrain);
    abstract int getResistBonus(Terrain terrain);

    /**
     * Returns whether the health is 0 or below or not.
     * @return
     */
    public boolean isDead() {
        return (health == 0);
    }

    /**
     * Returns the name of the units class.
     * @return
     */
    public String getUnitName() {
        return getClass().getSimpleName();
    }

    /**
     * Sets the health of the unit.
     * @param health
     */
    public void setHealth(int health) {
        if(health<0){
            health = 0;
        }
        this.health = health;
    }
    public void revive(){
        health = totalHealth;
    }

    /**
     * oppHealth variable calculates the given opponents health after the attack. Then the method sets the new health for the opponent.
     * @param opponent The opponent that is being attacked.
     */
    public int attack(Unit opponent, Terrain terrain){
        if(!opponent.isDead()) {
            int dmg = opponent.getHealth();
            int oppHealth = opponent.getHealth() - (this.attack + this.getAttackBonus(terrain)) + (opponent.getArmor() + opponent.getResistBonus(terrain));
            dmg = dmg - oppHealth;
            opponent.setHealth(oppHealth);
            opponent.attacksReceived++;
            return dmg;
        }
        else{
            throw new IllegalArgumentException("Unit being attacked is already dead");
        }
    }

    /**
     * Method that returns a String formatted in a csv format.
     * @return
     */
    public String toCSVFormat(){
        return this.getClass().getSimpleName() + "," + this.getName() + "," + this.getHealth();
    }

    @Override
    public String toString() {
        return "Unit{" +
                "name='" + name + '\'' +
                ", health=" + health +
                ", attack=" + attack +
                ", armor=" + armor +
                '}' + "\n" ;
    }


}
