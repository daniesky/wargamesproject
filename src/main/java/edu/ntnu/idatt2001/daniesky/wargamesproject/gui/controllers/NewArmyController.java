package edu.ntnu.idatt2001.daniesky.wargamesproject.gui.controllers;

import edu.ntnu.idatt2001.daniesky.wargamesproject.fileHandling.FileHandler;
import edu.ntnu.idatt2001.daniesky.wargamesproject.gui.FXMLLoaderClass;
import edu.ntnu.idatt2001.daniesky.wargamesproject.unit.Army;
import edu.ntnu.idatt2001.daniesky.wargamesproject.unit.Unit;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.AnchorPane;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import org.controlsfx.control.PopOver;
import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;

/**
 * Controller for GUI page that creates a new army within the "my armies" tab.
 */
public class NewArmyController implements Initializable{

    @FXML private TextField nameLabel;
    @FXML private TableView<Unit> armyTable;
    @FXML private TableColumn unitColumn;
    @FXML private TableColumn nameColumn;
    @FXML private TableColumn hpColumn;
    @FXML private TableColumn atColumn;
    @FXML private TableColumn arColumn;
    @FXML private Button addUnits;

    private String path;

    public static ObservableList<Unit> unitData = FXCollections.observableArrayList();

    /**
     * Initialises the unit table.
     * @param url
     * @param resourceBundle
     */
    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        //Initalize tables, so that they can retrieve correct information for Unit class.
        nameColumn.setCellValueFactory(new PropertyValueFactory<>("name"));
        hpColumn.setCellValueFactory(new PropertyValueFactory<>("health"));
        atColumn.setCellValueFactory(new PropertyValueFactory<>("attack"));
        arColumn.setCellValueFactory(new PropertyValueFactory<>("armor"));
        unitColumn.setCellValueFactory(new PropertyValueFactory<>("unitName"));
    }

    /**
     * Refreshes table when updates happen.
     */
    public void refreshTable(){
        armyTable.setItems(unitData);
    }

    @FXML
    public void addUnitsPopUp(ActionEvent actionEvent) throws IOException {
        AddUnitsPopUpController.army = 5;
        AnchorPane content = new AnchorPane();
        PopOver popOver = new PopOver();
        URL url = getClass().getClassLoader().getResource("addUnitsPopUp.fxml");
        content.getChildren().setAll((Node) FXMLLoader.load(url));
        popOver.setContentNode(content);
        popOver.show(addUnits);
        refreshTable();
    }
    /**
     * Deletes a unit from table and army.
     * @param actionEvent
     */
    @FXML
    public void delete(ActionEvent actionEvent){
        Unit unit = armyTable.getSelectionModel().getSelectedItem();
        unitData.remove(unit);
        refreshTable();
    }
    /**
     * Done button pressed, army information is saved and you return to My Armies.
     * @param actionEvent
     * @throws IOException  if FXML loading error occurs.
     */
    @FXML
    public void doneEdit(ActionEvent actionEvent) throws IOException {
        FileHandler fileHandler = new FileHandler("Armies/");
        ArrayList<Unit> units= new ArrayList<>(unitData);
        try {
            Army army = new Army(nameLabel.getText(), units);
            army.setImportPath(path);
            fileHandler.createArmyFile(army);
            FXMLLoaderClass.goToMyArmies(actionEvent);
        }
        catch(IllegalArgumentException e){
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Error occured");
            alert.setHeaderText("Army is missing something.");
            alert.setContentText("Make sure army has name and atleast one unit. ");
            alert.showAndWait();
        }
    }
    /**
     * Imports a new army from file.
     * @param actionEvent
     */
    @FXML
    public void importArmy(ActionEvent actionEvent){
        Stage stage = new Stage();
        FileChooser fileChooser = new FileChooser();
        fileChooser.setTitle("Open Army File");
        File f = fileChooser.showOpenDialog(stage);
        FileHandler fileHandler = new FileHandler("Armies/");
        try {
            Army army = fileHandler.readArmyFile(f);
            if(army != null) {
                path = army.getPath();
                unitData.clear();
                unitData.addAll(army.getUnits());
                nameLabel.setText(army.getName());
                refreshTable();
            }
        }catch(IllegalArgumentException e){
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Error occured");
            alert.setHeaderText("An error occured while reading the file");
            alert.setContentText("Something is wrong with uploaded file. It might be corrupt or in the wrong format. ");
            alert.showAndWait();
        }
        //Catches exception thrown for null files.
        catch(NullPointerException e){

        }
    }
    /**
     * Goes to the home page.
     * @param actionEvent
     * @throws IOException  if FXML loading error occurs.
     */
    @FXML
    public void home(ActionEvent actionEvent) throws IOException {
        FXMLLoaderClass.goToHome(actionEvent);
    }

    /**
     * Goes back to the "my armies" page.
     * @param actionEvent
     * @throws IOException
     */
    @FXML
    public void cancel(ActionEvent actionEvent) throws IOException{
        FXMLLoaderClass.goToMyArmies(actionEvent);
    }
}
