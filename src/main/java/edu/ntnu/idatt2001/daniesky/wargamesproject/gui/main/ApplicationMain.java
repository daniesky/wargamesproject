package edu.ntnu.idatt2001.daniesky.wargamesproject.gui.main;

/**
 * Class that is needed when building application to jar file.
 */
public class ApplicationMain {
    /**
     * Main method calls the main method from Application class.
     * @param args args
     */
    public static void main(String[] args){
        Application.main(args);
    }
}
