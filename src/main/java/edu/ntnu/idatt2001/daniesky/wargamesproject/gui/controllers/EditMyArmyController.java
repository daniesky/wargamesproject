package edu.ntnu.idatt2001.daniesky.wargamesproject.gui.controllers;

import edu.ntnu.idatt2001.daniesky.wargamesproject.fileHandling.FileHandler;
import edu.ntnu.idatt2001.daniesky.wargamesproject.gui.FXMLLoaderClass;
import edu.ntnu.idatt2001.daniesky.wargamesproject.unit.Army;
import edu.ntnu.idatt2001.daniesky.wargamesproject.unit.Unit;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.AnchorPane;
import org.controlsfx.control.PopOver;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

/**
 * Class that controls the "editMyArmy" fxml file. This controller handles the editing of armies within "my armies".
 */
public class EditMyArmyController implements Initializable{

    //Static army variable that is used by other controllers to set which army is being edited.
    public static Army army;

    @FXML private TextField nameLabel;
    @FXML private TableView<Unit> armyTable;
    @FXML private TableColumn unitColumn;
    @FXML private TableColumn nameColumn;
    @FXML private TableColumn hpColumn;
    @FXML private TableColumn atColumn;
    @FXML private TableColumn arColumn;
    @FXML private Button addUnits;

    //List that AddUnitsPopUpController adds units to.
    public static ObservableList<Unit> unitData = FXCollections.observableArrayList();

    /**
     * Initialises table, fills name label and adds units to list.
     * @param url
     * @param resourceBundle
     */
    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        nameLabel.setText(army.getName());
        nameColumn.setCellValueFactory(new PropertyValueFactory<>("name"));
        hpColumn.setCellValueFactory(new PropertyValueFactory<>("health"));
        atColumn.setCellValueFactory(new PropertyValueFactory<>("attack"));
        arColumn.setCellValueFactory(new PropertyValueFactory<>("armor"));
        unitColumn.setCellValueFactory(new PropertyValueFactory<>("unitName"));
        unitData.clear();
        unitData.addAll(army.getUnits());
        refreshTable();
    }

    /**
     * Refreshes table when updates happen.
     */
    public void refreshTable(){
        armyTable.setItems(unitData);
    }
    @FXML
    public void addUnitsPopUp(ActionEvent actionEvent) throws IOException {
        AddUnitsPopUpController.army = 4;
        AnchorPane content = new AnchorPane();
        PopOver popOver = new PopOver();
        URL url = getClass().getClassLoader().getResource("addUnitsPopUp.fxml");
        content.getChildren().setAll((Node) FXMLLoader.load(url));
        popOver.setContentNode(content);
        popOver.show(addUnits);
        refreshTable();
    }


    /**
     * Deletes a unit from table and army.
     * @param actionEvent
     */
    @FXML
    public void delete(ActionEvent actionEvent){
        Unit unit = armyTable.getSelectionModel().getSelectedItem();
        army.remove(unit);
        unitData.remove(unit);
        refreshTable();
    }
    /**
     * Done button pressed, army information is saved and you return to My Armies.
     * @param actionEvent
     * @throws IOException  if FXML loading error occurs.
     */
    @FXML
    public void doneEdit(ActionEvent actionEvent) throws IOException {
        FileHandler fileHandler = new FileHandler("Armies/");
        fileHandler.updateFile(army);
        FXMLLoaderClass.goToMyArmies(actionEvent);
    }

    /**
     * Revives all units in the table.
     * @param actionEvent
     */
    @FXML
    public void revive(ActionEvent actionEvent){
        army.reviveAll();
        unitData.clear();
        unitData.addAll(army.getUnits());
        refreshTable();
    }
}
