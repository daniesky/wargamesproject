package edu.ntnu.idatt2001.daniesky.wargamesproject.battle;
import edu.ntnu.idatt2001.daniesky.wargamesproject.unit.Army;
import edu.ntnu.idatt2001.daniesky.wargamesproject.unit.CavalryUnit;
import edu.ntnu.idatt2001.daniesky.wargamesproject.unit.CommanderUnit;
import edu.ntnu.idatt2001.daniesky.wargamesproject.unit.InfantryUnit;
import edu.ntnu.idatt2001.daniesky.wargamesproject.unit.RangedUnit;

import java.util.Scanner;

/**
 * A class for a client that can be used to run simulations in the console. Has a preset simulation, and ability to
 * create new armies.
 */
public class BattleClient {
    private static Scanner sc = new Scanner(System.in);
    public static void main(String[] args){
    BattleClient client = new BattleClient();
    client.run();
    }

    /**
     * The run method has the primary function of the client.
     */
    public void run(){
        System.out.println(" --- Welcome to the battle simulator ---");
        System.out.println(" 1. Create your own armies \n 2. Run with already created armies. \n");
        int in = sc.nextInt();
        if(in == 2){
            sampleArmies();
        }
        else if(in == 1) {
            createArmies();
        }
    }

    /**
     * The menu for adding troops.
     * @return
     */
    private int addTroopMenu(){
        System.out.println("--- Add troops ---");
        System.out.println("1. Add infantry units");
        System.out.println("2. Add cavalry units");
        System.out.println("3. Add ranged units");
        System.out.println("4. Add commander units");
        System.out.println("5. Finished");
        int choice = sc.nextInt();
        sc.nextLine();
        return choice;
    }

    /**
     * The loop that runs as the client creates armies.
     * @param army
     */
    private void troopLoop(Army army){
        boolean finished = false;
        while(!finished){
            int choice = addTroopMenu();
            switch (choice) {
                case 1 -> {
                    System.out.println("Name the infantry unit");
                    String unitName = sc.nextLine();
                    System.out.println("How much health?");
                    int health = sc.nextInt();
                    System.out.println("How many units?");
                    int unitNr = sc.nextInt();
                    for (int i = 0; i < unitNr; i++) {
                        army.add(new InfantryUnit(unitName, health));
                    }
                }
                case 2 -> {
                    System.out.println("Name the cavalry unit");
                    String unitName = sc.nextLine();
                    System.out.println("How much health?");
                    int health = sc.nextInt();
                    System.out.println("How many units?");
                    int unitNr = sc.nextInt();
                    for (int i = 0; i < unitNr; i++) {
                        army.add(new CavalryUnit(unitName, health));
                    }
                }
                case 3 -> {
                    System.out.println("Name the ranged unit");
                    String unitName = sc.nextLine();
                    System.out.println("How much health?");
                    int health = sc.nextInt();
                    System.out.println("How many units?");
                    int unitNr = sc.nextInt();
                    for (int i = 0; i < unitNr; i++) {
                        army.add(new RangedUnit(unitName, health));
                    }
                }
                case 4 -> {
                    System.out.println("Name the commander unit");
                    String unitName = sc.nextLine();
                    System.out.println("How much health?");
                    int health = sc.nextInt();
                    System.out.println("How many units?");
                    int unitNr = sc.nextInt();
                    for (int i = 0; i < unitNr; i++) {
                        army.add(new CommanderUnit(unitName, health));
                    }
                }
                case 5 -> {
                    finished = true;
                }
            }
        }
    }

    /**
     * Creates two sample armies that are used in this client as a sample simulation.
     */
    private void sampleArmies(){
        Army human = new Army("Human army");
        Army orc = new Army("Orcish horde");
        int i = 0;
        human.add(new CommanderUnit("Mountain King",180));
        orc.add(new CommanderUnit("Gul dan",180));
        while(i<500){
            human.add(new InfantryUnit("Footman",100));
            orc.add(new InfantryUnit("Grunt",100));
            if(i<100){
                human.add(new CavalryUnit("Knight",100));
                orc.add(new CavalryUnit("Raider",100));
            }
            if(i<200){
                human.add(new RangedUnit("Archer",100));
                orc.add(new RangedUnit("Spearman",100));
            }
            i++;
        }
        Battle battle = new Battle(human,orc,Terrain.PLAINS);
        System.out.println(battle.simulate(false,true));
    }

    /**
     * Run method calls this method when user wants to create their own armies. Armies are created and then simulated.
     */
    private void createArmies(){
        System.out.println(" --- Lets start creating the first army");
        System.out.print("Army name: ");
        String name = sc.nextLine();
        Army armyOne = new Army(name);
        System.out.println("--- Now lets add troops to the first army --- ");
        troopLoop(armyOne);
        System.out.println("--- Now the second army");
        System.out.print("Army name: ");
        String nameTwo = sc.nextLine();
        Army armyTwo = new Army(nameTwo);
        troopLoop(armyTwo);
        System.out.println("--- Now the battle begins! --- ");
        Battle battle = new Battle(armyOne, armyTwo,Terrain.PLAINS);
        System.out.println(battle.simulate(false,true));
    }

}
