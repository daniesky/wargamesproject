package edu.ntnu.idatt2001.daniesky.wargamesproject.unit;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Random;
import java.util.stream.Collectors;

/**
 * Class describes a collection of different units in an army. Because it is a collection of units
 * it is in the unit package.
 */
public class Army {
    private String name;
    private ArrayList<Unit> units;

    //String that is initialized if army gets created by file.
    private String importPath;

    /**
     * Constructor for the army class for an already existing list of units.
     * @param name
     * @param units
     */
    public Army(String name, ArrayList<Unit> units) {
        if(name.isBlank()){
            throw new IllegalArgumentException("Name can not be blank.");
        }
        this.name = name;
        if(units.isEmpty()){
            throw new IllegalArgumentException("Unit list can not be empty.");
        }
        this.units = units;
    }

    /**
     * Constructor used to make deep copies of this class.
     * @param army
     */
    public Army(Army army){
        if(army.getName().isBlank()){
            throw new IllegalArgumentException("Name can not be blank.");
        }
        this.name = army.getName();
        if(army.getUnits().isEmpty()){
            throw new IllegalArgumentException("Unit list can not be empty.");
        }
        this.units = army.getUnits();
        if(army.getPath() != null){
            this.importPath = army.getPath();
        }
    }

    /**
     * Constructor for army class that creates new list.
     * @param name
     */
    public Army(String name) {
        if(name.isBlank()){
            throw new IllegalArgumentException("Name can not be blank.");
        }
        this.name = name;
        this.units = new ArrayList<>();
    }

    /**
     * Returns the name of the army.
     * @return
     */
    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }
    public void setImportPath(String importPath) {
        this.importPath = importPath;
    }
    public String getPath() {
        return importPath;
    }
    public ArrayList<Unit> getUnits() {
        return units;
    }
    public int getSize(){
        return this.units.size();
    }

    /**
     * Adds a new unit to the list.
     * @param unit
     */
    public void add(Unit unit){
            units.add(unit);
    }

    /**
     * Adds all units in a list of units to the army.
     * @param units
     */
    public void addAll(ArrayList<Unit> units){
        this.units.addAll(units);
    }

    /**
     * Gets the death toll of a specific unit type.
     * @param unitType
     * @return
     */
    public int getUnitDeathToll(String unitType){
        if(unitType.equals("all")){
            return (int) units.stream()
                    .filter(Unit -> Unit.isDead())
                    .count();
        }
        else {
            return (int) units.stream()
                    .filter(Unit -> Unit.getUnitName().equals(unitType))
                    .filter(Unit -> Unit.isDead())
                    .count();
        }
    }

    /**
     * Gets the count of living units within one type.
     * @param unitType
     * @return
     */
    public int getLiving(String unitType){
        if(unitType.equals("all")){
            return (int) units.stream()
                    .filter(Unit -> !Unit.isDead())
                    .count();
        }
        else {
            return (int) units.stream()
                    .filter(Unit -> Unit.getUnitName().equals(unitType))
                    .filter(Unit -> !Unit.isDead())
                    .count();
        }
    }
    /**
     * Removes a unit from the army.
     * @param unit
     */
    public void remove(Unit unit){
        units.remove(unit);
    }

    /**
     * Boolean that returns true when the army has living troops.
     * @return
     */
    public boolean hasLivingUnits(){
        return this.units.stream()
                .filter(Unit -> !Unit.isDead())
                .count() != 0;
    }

    /**
     * Returns a random unit from the army.
     * @return
     */
    public Unit getRandom(){
        boolean run = true;
        Unit unit;
        Random rand = new Random();
        int index;
        if(!this.hasLivingUnits()){
            throw new IllegalArgumentException("This army does not have any living troops");
        }
        while(run) {
            index = rand.nextInt(units.size());
            unit = units.get(index);
            if(!unit.isDead()){
                run = false;
                return unit;
            }
        }
        return null;
    }

    /**
     * @return Returns a list with all infantry units.
     */
    public List<Unit> getInfantryUnits(){
        List<Unit> infantryUnits = units.stream()
                .filter(item -> item instanceof InfantryUnit)
                .collect(Collectors.toList());
        return infantryUnits;
    }
    /**
     * @return Returns a list with all cavaly units.
     */
    public List<Unit> getCavalryUnits(){
        List<Unit> cavalryUnits = units.stream()
                .filter(item -> item instanceof CavalryUnit)
                .filter(item -> !(item instanceof CommanderUnit))
                .collect(Collectors.toList());
        return cavalryUnits;
    }

    /**
     * @return Returns a list with all ranged units.
     */
    public List<Unit> getRangedUnits(){
        List<Unit> rangedUnits = units.stream()
                .filter(item -> item instanceof RangedUnit)
                .collect(Collectors.toList());
        return rangedUnits;
    }
    /**
     * @return Returns a list with all commander units.
     */
    public List<Unit> getCommanderUnits(){
        List<Unit> CommanderUnits = units.stream()
                .filter(item -> item instanceof CommanderUnit)
                .collect(Collectors.toList());
        return CommanderUnits;
    }

    /**
     * Revives all dead units within army.
     */
    public void reviveAll(){
        units.stream()
                .forEach(Unit -> Unit.revive());
    }
    @Override
    public String toString() {
        return "Army" +
                "name='" + name + '\'' +
                "units=" + units.toString();
    }
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Army)) return false;
        Army army = (Army) o;
        return Objects.equals(name, army.name) && Objects.equals(units, army.units);
    }
    @Override
    public int hashCode() {
        return Objects.hash(name, units);
    }
}
