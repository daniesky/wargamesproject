package edu.ntnu.idatt2001.daniesky.wargamesproject.unit;

import java.util.ArrayList;
import java.util.Locale;

/**
 * Factory design pattern that creates different units, and specified amount of different units.
 */
public class UnitFactory {

    /**
     * Method creates a given unit with name and health given as parameters.
     * @param unitType
     * @param name
     * @param health
     * @return
     */
    public static Unit createUnit(String unitType, String name, int health){
        if(unitType == null){
            return null;
        }
        //Make sure that input is universal.
        unitType = unitType.toUpperCase(Locale.ROOT);
        unitType = unitType.replaceAll(" ", "");

        if(unitType.equals("INFANTRYUNIT")){
            return new InfantryUnit(name,health);
        }
        else if(unitType.equals("RANGEDUNIT")){
            return new RangedUnit(name,health);
        }
        else if(unitType.equals("CAVALRYUNIT")){
            return new CavalryUnit(name,health);
        }
        else if(unitType.equals("COMMANDERUNIT")){
            return new CommanderUnit(name,health);
        }
        return null;
    }

    /**
     * Method creates a given amount of units. Unit type, name, health and amount of units is passed as parameter.
     * @param unitType
     * @param name
     * @param health
     * @param n
     * @return
     */
    public static ArrayList<Unit> createUnits(String unitType, String name, int health, int n){
        ArrayList<Unit> units = new ArrayList<>();
        if(unitType == null){
            return null;
        }
        //Make sure that input is universal.
        unitType = unitType.toUpperCase(Locale.ROOT);
        unitType = unitType.replaceAll(" ", "");

        if(unitType.equals("INFANTRYUNIT")){
            for(int i = 0; i<n;i++){
                units.add(new InfantryUnit(name,health));
        }
            return units;
        }
        else if(unitType.equals("RANGEDUNIT")){
            for(int i = 0; i<n;i++){
                units.add(new RangedUnit(name,health));
            }
            return units;
        }
        else if(unitType.equals("CAVALRYUNIT")){
            for(int i = 0; i<n;i++){
                units.add(new CavalryUnit(name,health));
            }
            return units;
        }
        else if(unitType.equals("COMMANDERUNIT")){
            for(int i = 0; i<n;i++){
                units.add(new CommanderUnit(name,health));
            }
            return units;
        }
        return null;
    }
    }
