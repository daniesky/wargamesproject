package edu.ntnu.idatt2001.daniesky.wargamesproject.gui.models;

import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;

/**
 * This class is a model representing a summary of a unitType within an army. Contains the  unit type, how many of said  unit type
 * and how many are alive.
 */
public class ArmySummaryModel {
    private SimpleStringProperty unitType;
    private SimpleIntegerProperty amount;
    private SimpleIntegerProperty deaths;

    /**
     * Constructor that takes the unitType
     * @param unitType
     * @param amount
     * @param deaths
     */
    public ArmySummaryModel(String unitType, int amount, int deaths) {
        this.unitType = new SimpleStringProperty(unitType);
        this.amount = new SimpleIntegerProperty(amount);
        this.deaths = new SimpleIntegerProperty(deaths);
    }

    public int getAmount() {
        return amount.get();
    }
    public int getDeaths() {
        return deaths.get();
    }
    public String getUnitType() {
        return unitType.get();
    }
    public void setAmount(int amount) {
        this.amount.set(amount);
    }
    public void setDeaths(int deaths) {
        this.deaths.set(deaths);
    }
    public void setUnitType(String unitType) {
        this.unitType.set(unitType);
    }
}
