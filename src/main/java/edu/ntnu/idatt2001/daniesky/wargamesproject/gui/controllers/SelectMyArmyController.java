package edu.ntnu.idatt2001.daniesky.wargamesproject.gui.controllers;

import edu.ntnu.idatt2001.daniesky.wargamesproject.fileHandling.FileHandler;
import edu.ntnu.idatt2001.daniesky.wargamesproject.unit.Army;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.ResourceBundle;

/**
 * Controller class for pop up windowing displaying all armies in "my armies". Is used within the CreateArmiesController
 * to import previously created armies.
 */
public class SelectMyArmyController implements Initializable{

    @FXML private TableView<Army> armies;
    @FXML private TableColumn<Army, String> armyName;
    @FXML private TableColumn<Army, Integer> armySize;
    private ObservableList<Army> unitData = FXCollections.observableArrayList();

    //Static variables that CreateArmiesController interacts with.
    public static Army selectedArmy;
    public static int selectedTable;

    /**
     * Method initializes table and retrieves armies from file registry.
     * @param url
     * @param resourceBundle
     */
    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        armyName.setCellValueFactory(new PropertyValueFactory<>("name"));
        armySize.setCellValueFactory(new PropertyValueFactory<>("size"));
        try {
            retrieveArmies();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Private method that retrieves army from the Armies file folder, creates the armies and adds them to the table.
     * @throws IOException  if file loading error occurs.
     */
    private void retrieveArmies() throws IOException {
        unitData.clear();
        FileHandler f = new FileHandler("Armies/");
        ArrayList<Army> armies = new ArrayList<>();
        Files.walk(Paths.get("Armies"))
                .filter(Files::isRegularFile)
                .forEach(Path -> armies.add(f.readArmyFile(new File(Path.toString()))));
        unitData.addAll(armies);
        refreshTable();
    }

    /**
     * Takes the static unitData and adds all items to the table. This is done after new armies are added/deleted or edited.
     */
    private void refreshTable(){
        armies.setItems(unitData);
    }

    /**
     * When done button is clicked, if army is selected, selectedArmy is set and then popover is hidden, calling method
     * within CreateArmiesController to import selectedArmy into selected table.
     * @param actionEvent
     * @throws IOException  if FXML loading error occurs.
     */
    @FXML
    public void done(ActionEvent actionEvent) throws IOException {
        if(!armies.getSelectionModel().isEmpty()){
            selectedArmy = armies.getSelectionModel().getSelectedItem();
        }
        else{
            selectedArmy = null;
        }
        CreateArmiesController.importPopOver.hide();
    }
}
