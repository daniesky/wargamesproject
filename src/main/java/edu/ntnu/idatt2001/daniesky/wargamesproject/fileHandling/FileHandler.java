package edu.ntnu.idatt2001.daniesky.wargamesproject.fileHandling;

import edu.ntnu.idatt2001.daniesky.wargamesproject.unit.*;
import java.io.*;
import java.util.Optional;

/**
 * Class that is created to manage files representing armies. Can create, read, overwrite and delete army files.
 */
public class FileHandler {

    private String dirPath;

    /**
     * Constructor for file handler class. Takes only one argument, which is the directory where files are
     * stored.
     * @param dirPath
     */
    public FileHandler(String dirPath) {
        this.dirPath = dirPath;
    }

    /**
     * Method for reading an army from a csv file. Throws exception for empty files, wrong format files etc.
     * @param file Input of a file
     * @return Returns a new army with the units and name from given csv.file.
     */
    public Army readArmyFile(File file){
        if(file == null){
            throw new NullPointerException("File is null");
        }
        //Verifies that file has some form of content.
        if(file.length() == 0){
            throw new IllegalArgumentException("File is empty");
        }
        //Verifies that input file is a csv file.
        if(!this.verifyCSV(file.getName())){
            throw new IllegalArgumentException("Wrong file type, must be csv file. ");
        }

        try(FileReader fileReader = new FileReader(file)){
            //Using BufferedReader as it is faster and more efficient.
            BufferedReader csvReader = new BufferedReader(fileReader);
            String name = csvReader.readLine();

            //BufferedReader will read any whitespace line as a blank String, this while loop runs through all
            // whitespace and retrieves the army name.
            while(name.isBlank()){
                name = csvReader.readLine();
            }

            //Purpose of the row counter is to deliver feedback on where errors were found in the csv files.
            int rowCounter = 0;

            Army readArmy = new Army(name);
            rowCounter++;
            String row;
            Unit newUnit = null;

            //While there is a line to read.
            while((row = csvReader.readLine())!=null) {
                rowCounter++;
                try {

                    //Separates string into array of strings.
                    String[] unitData = row.split(",");
                    String unitType = unitData[0];
                    String unitName = unitData[1];
                    int unitHealth = Integer.parseInt(unitData[2]);

                    // Reads the type of unit and creates an instance of that class.

                    newUnit = UnitFactory.createUnit(unitType,unitName,unitHealth);
                    if(newUnit != null){
                        readArmy.add(newUnit);
                    }
                }

                //Catches the most common errors to occur when reading, and gives feedback on where the error was found.
                //This will allow the reader to extract some data even if some lines are corrupted.
                catch (ArrayIndexOutOfBoundsException|IllegalArgumentException|NullPointerException e){
                    System.out.println("Error found in line " + rowCounter + ". Line has been skipped.");
                }
            }
            readArmy.setImportPath(file.getAbsolutePath());
            return readArmy;
        }
        //Catches error that can occur when trying to read file.
        catch(IOException e){
            e.toString();
        }
        return null;
    }

    /**
     * Creates a file with the army name and the info on the Units in the csv format, and saves it in the specified directory.
     * Calls on a private method to do the writing of the information.
     * Returns true if the file was successfully created, and false if any error occured. This will most likely be that
     * the file already exists.
     * @return
     */
    public boolean createArmyFile(Army army) {
        try {
            // Removes all spaces from name as to make it suitable for filename.
            String name = army.getName().replaceAll(" ", "");
            name = dirPath + name + ".csv";
            // Creates the file.
            File armyFile = new File(name);
            army.setImportPath(armyFile.getAbsolutePath());
            // Check if file is already created. If so, then all text is overridden with new army.
            if (armyFile.isFile()) {
                throw new IllegalArgumentException("Army already exists");
            } else {
                boolean wasCreated = armyFile.createNewFile();
                // If file was successfully created, call on method to write army to file.,
                if (wasCreated) {
                    this.writeToArmyFile(armyFile, army);
                    return true;
                } else {
                    return false;
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return false;
    }
    /**
     * public method for writing to a given File. This writes the information about the units onto the file given
     * in parameter.
     * @param armyFile
     */
    public void writeToArmyFile(File armyFile, Army army){
        try {
            PrintWriter pw = new PrintWriter(new FileWriter(armyFile,false));
            pw.println(army.getName());
            army.getUnits().forEach(unit -> pw.println(unit.toCSVFormat()));
            pw.close();

        }
        //Catches exception when printing to given file.
        catch(IOException e){
            throw new IllegalArgumentException("Could not print to specified File");
        }

    }

    /**
     * Method used to update an already existing army file.
     * @param army
     */
    public void updateFile(Army army){

        File f = new File(army.getPath());

        // Removes all spaces from name as to make it suitable for filename.
        String name = army.getName().replaceAll(" ", "");
        name = f.getParent()+ "/" + name + ".csv";
        // Creates the new file destination.
        File armyFile = new File(name);
        army.setImportPath(armyFile.getAbsolutePath());

        f.renameTo(armyFile);

        this.writeToArmyFile(armyFile,army);

    }

    /**
     * Method for getting the extension of a file name. This is to verify that given files are csv files.
     * @param filename
     * @return
     */
    private Optional<String> getExtensionByStringHandling(String filename) {
        return Optional.ofNullable(filename)
                .filter(f -> f.contains("."))
                .map(f -> f.substring(filename.lastIndexOf(".") + 1));
    }

    /**
     * Deletes a file connected to an army. Used when an army is deleted from my armies.
     * @param army
     */
    public void deleteFile(Army army){
        try {
            File f = new File(army.getPath());
            f.delete();
        }catch(NullPointerException e){
            throw new NullPointerException("Could not find file.");
        }
    }

    /**
     * Static method that controls if the used army directory exists, if not creates the directory.
     */
    public static void controlDirectory(){
        File controlFile = new File("Armies");
        if(!controlFile.exists()){
            controlFile.mkdir();
        }
    }

    /**
     * Method that gets the optional String with the extension from getExtensionByStringHandling method, and returns true
     * if file is csv.
     * @param filename
     * @return
     */
    private boolean verifyCSV(String filename){
        String extension = this.getExtensionByStringHandling(filename).get();
        return(extension.equals("csv"));
    }
}
